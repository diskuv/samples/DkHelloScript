# DkHelloScript

> DkSDK: the dev kit to tame the complexity of multi-platform/tier/language software

DkCoder gets you a dev experience that is productive like Python, but enhanced so you:

1. have nothing to install except Visual Studio Code
2. have safe and understandable code ("static typing")
<!-- 3. have almost no need to register with a central "package" repository when sharing your code
4. (coming later) can distribute single-file, performance optimized executables
5. (coming later) can give your game users or business customers the ability to use Visual Studio Code to customize your C and Java applications ("embedding") -->

The [DkCoder - Intro to Scripting](https://diskuv.com/dksdk/coder/2024-intro-scripting/) article is the home of the documentation.

## Running the documentation

```sh
./dk DkHelloScript_Std.Y33Article --serve
```

Your local documentation will be available at: <http://127.0.0.1:8080>

## Navigation

- `src/DkHelloScript_Std/And*` - Getting Started series.
- `src/DkHelloScript_Std/B*` - Beginner series. Start here.
- `src/DkHelloScript_Std/J*` - Junior series. Only do once **all** of the beginner content is done.
- `src/DkHelloScript_Std/Y*` - Articles and tests for HelloScript.

> The capital letters `I`, `O`, `S`, and `Z` are sometimes confused for `1`, `0`, `5` and `2` so those are often skipped.
