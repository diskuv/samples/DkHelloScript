(** This is the documentation for the DkHelloScript_Std library defined
    as a top comment in a ["src/DkHelloScript_Std/lib__.ml"] file.
    
    If you have code like:
    
    {[
      open DkHelloScript_Std
    ]}

    then you can hover over the [DkHelloScript_Std] and see this
    documentation.
    
    You will have needed to run that script first, as in:

    {v
    ./dk DkHelloScript_Std.AndHello
    v}
    *)
