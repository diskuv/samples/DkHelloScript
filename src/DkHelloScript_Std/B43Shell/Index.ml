(* The presence of this module is an early tech limitation of DkCoder:
   DkCoder today can't descend through two consecutive stitch modules.
   So references to B43Shell.B35Shexp.B43Countdown from a higher level
   will fail. *)
module B35Shexp = B35Shexp
