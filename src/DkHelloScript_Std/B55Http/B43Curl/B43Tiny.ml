open Lwt.Syntax
open Tr1Stdlib_V414CRuntime
open Tr1Stdlib_V414Base
module Curl = Cohttp_curl_lwt (* This will change to a Tr1 namespace! *)

let client uri ofile meth' =
  Format.eprintf "Client with URI %s@." (Uri.to_string uri) ;
  let meth = Http.Method.of_string meth' in
  Format.eprintf "Client %s issued@." meth' ;
  let reply =
    let context = Curl.Context.create () in
    let request =
      Curl.Request.create ~timeout_ms:5000 meth ~uri:(Uri.to_string uri)
        ~input:Curl.Source.empty ~output:Curl.Sink.string
    in
    Curl.submit context request
  in
  let* resp, response_body =
    Lwt.both (Curl.Response.response reply) (Curl.Response.body reply)
  in
  Format.eprintf "response:%a@." Http.Response.pp resp ;
  let status = Http.Response.status resp in
  Format.eprintf "Client %s returned: %a@." meth' Http.Status.pp status ;
  ( match status with
  | #Http.Status.success ->
      Format.eprintf "Status code was in the set 'success'@."
  | _ ->
      Format.eprintf "Status code was not in the set 'success'@." ) ;
  let len = String.length response_body in
  Format.eprintf "Client body length: %d@." len ;
  let output_body c = Lwt_io.write c response_body in
  match ofile with
  | None ->
      output_body Lwt_io.stdout
  | Some fname ->
      Lwt_io.with_file ~mode:Lwt_io.output fname output_body

let () =
  if Tr1EntryName.module_id = __MODULE_ID__ then
    (* The first optional argument is the OUTPUT_FILE. *)
    let ofile = if Array.length Sys.argv > 1 then Some Sys.argv.(1) else None in
    Lwt_main.run
      (client
         (Uri.of_string "https://jigsaw.w3.org/HTTP/h-content-md5.html")
         ofile "GET" )

(* most of this tutorial script comes from ISC-licensed
   https://github.com/mirage/ocaml-cohttp/blob/5efbcecfc3cdad5f536e0d5a55943a0b8561faaa/cohttp-curl-lwt/bin/curl.ml *)
