open Tr1Stdlib_V414CRuntime
open Tr1Stdlib_V414Base
module Encoding = Resto_json.Encoding
module Service = Resto.MakeService (Encoding)
module Directory = Resto_directory.Make (Encoding)
module Media_type = Resto_cohttp.Media_type.Make (Encoding)

let json =
  { Media_type.name= Cohttp.Accept.MediaType ("application", "json")
  ; q= Some 1000
  ; pp=
      (fun _enc ppf raw ->
        let json = Ezjsonm.from_string raw in
        Format.fprintf ppf "%s" (Ezjsonm.to_string json) )
  ; construct=
      (fun enc v ->
        Ezjsonm.value_to_string @@ Json_repr.Ezjsonm.repr
        @@ Json_encoding.construct enc v )
  ; construct_seq=
      (fun enc v ->
        let item =
          Ezjsonm.value_to_string @@ Json_repr.Ezjsonm.repr
          @@ Json_encoding.construct enc v
        in
        Seq.return (Bytes.of_string item, 0, String.length item) )
  ; destruct=
      (fun enc body ->
        let json = Ezjsonm.from_string body in
        try Ok (Json_encoding.destruct enc json)
        with exc -> Error (Printexc.to_string exc) ) }

let media_types = [json]

module Logger : Resto_cohttp_server_lwt.Server_lwt.LOGGING = struct
  let debug fmt = Format.kasprintf (Format.printf "[DEBUG]: %s\n%!") fmt

  let log_info fmt = Format.kasprintf (Printf.printf "[INFO]: %s\n%!") fmt

  let log_notice fmt = Format.kasprintf (Printf.printf "[NOTICE]: %s\n%!") fmt

  let warn fmt = Format.kasprintf (Printf.printf "[WARN]: %s\n%!") fmt

  let log_error fmt = Format.kasprintf (Printf.eprintf "[ERROR]: %s\n%!") fmt

  let lwt_debug fmt =
    Format.kasprintf Lwt_fmt.(fprintf stdout "[DEBUG]: %s\n%!") fmt

  let lwt_log_info fmt =
    Format.kasprintf Lwt_fmt.(fprintf stdout "[INFO]: %s\n%!") fmt

  let lwt_log_notice fmt =
    Format.kasprintf Lwt_fmt.(fprintf stdout "[NOTICE]: %s\n%!") fmt

  let lwt_warn fmt =
    Format.kasprintf Lwt_fmt.(fprintf stdout "[WARN]: %s\n%!") fmt

  let lwt_log_error fmt =
    Format.kasprintf Lwt_fmt.(fprintf stderr "[ERROR]: %s\n%!") fmt
end

let foo_bar =
  let open Resto.Path in
  List.fold_left add_suffix root ["foo"; "bar"]

let get_foo_bar =
  Service.get_service ~query:Resto.Query.empty ~output:Encoding.unit
    ~error:Encoding.unit foo_bar

let foo_blah =
  let open Resto.Path in
  List.fold_left add_suffix root ["foo"; "blah"]

let get_foo_blah =
  Service.get_service ~query:Resto.Query.empty ~output:Encoding.unit
    ~error:Encoding.unit foo_blah

let bwraf =
  let open Resto.Path in
  List.fold_left add_suffix root ["blue"; "white"; "red"; "and"; "fuchsia"]

let post_bwraf =
  Service.post_service ~query:Resto.Query.empty ~input:Encoding.unit
    ~output:Encoding.unit ~error:Encoding.unit bwraf

let directory =
  let open Directory in
  let dir = empty in
  let dir = register0 dir get_foo_bar (fun () () -> Lwt.return @@ `Ok ()) in
  let dir = register0 dir get_foo_blah (fun () () -> Lwt.return @@ `Ok ()) in
  let dir = register0 dir post_bwraf (fun () () -> Lwt.return @@ `Ok ()) in
  dir

let port = 8000

let uri = "http://localhost:8000"

let driver ~init_server ~launch ~set_acl ~shutdown =
  (* set up and start the server *)
  let ( let* ) = Lwt.bind in
  let* () =
    Logger.lwt_log_info
      "Starting server at http://localhost:%d. It will be alive for 30 \
       seconds. Try GET /foo/bar, GET /foo/blah and POST \
       /blue/white/red/and/fuchsia"
      port
  in
  let server = init_server ~media_types directory in
  let* () = launch server in
  (* stay alive for a bit *)
  let* () = Lwt_unix.sleep 15.0 in
  let* () = Logger.lwt_log_info "Switching to disallow any uri /foo/*" in
  set_acl server
  @@ Resto_acl.Acl.Allow_all
       {except= [{meth= Any; path= Exact [Literal "foo"; Wildcard]}]} ;
  (* stay alive for a bit *)
  let* () = Lwt_unix.sleep 15.0 in
  (* end of tests *)
  shutdown server

module Server_lwt = Resto_cohttp_server_lwt.Server_lwt.Make (Encoding) (Logger)

let () =
  Lwt_main.run
  @@ driver
       ~init_server:(fun ~media_types directory ->
         Server_lwt.init_server ~media_types directory )
       ~launch:(fun server ->
         Server_lwt.launch server Unix.(ADDR_INET (inet_addr_loopback, port)) )
       ~set_acl:Server_lwt.set_acl ~shutdown:Server_lwt.shutdown
