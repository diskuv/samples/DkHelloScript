(* [Tezt] and its submodule [Base] are designed to be opened.
   [Tezt] is the main module of the library and it only contains submodules,
   such as [Test] which is used below.
   [Tezt.Base] contains values such as [unit] which is used below. *)
open Tr1Tezt_C.Tezt
open Tr1Tezt_C.Tezt.Base

(* Register as many tests as you want like this. *)
let () =
  Test.register
  (* [~__FILE__] contains the name of the file in which the test is defined.
     It allows to select tests to run based on their filename. *)
    ~__FILE__
      (* Titles uniquely identify tests so that they can be individually selected. *)
    ~title:"demo"
      (* Tags are another way to group tests together to select them. *)
    ~tags:["string"; "concatenation"]
  @@ fun () ->
  let value, _ast =
    [%sample
      let a = "a"

      let b = "b"

      let eval () = a ^ b]
  in
  Check.((value = "ab") string) ~error_msg:"expected a ^ b = %R, got %L" ;
  unit

(* Call the main function of Tezt so that it actually runs your tests. *)
let () = if Tr1EntryName.module_id = __MODULE_ID__ then Test.run ()
