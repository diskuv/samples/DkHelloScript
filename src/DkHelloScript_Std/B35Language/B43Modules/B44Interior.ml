module A = struct
  let f () = 3

  module B = struct end
end

let (_ : int) = A.f ()

module C = struct end
