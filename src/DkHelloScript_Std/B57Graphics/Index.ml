(* The presence of this module is an early tech limitation of DkCoder:
   DkCoder today can't descend through two consecutive stitch modules.
   So references to B57Graphics.B43Bogue.B43Tiny from a higher level
   will fail. *)
module B43Bogue = B43Bogue
