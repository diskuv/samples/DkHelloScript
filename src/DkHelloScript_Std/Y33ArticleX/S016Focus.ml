[@@@warning "-unused-open"]

open Tr1Tezt_C.Tezt
open Tr1Tezt_C.Tezt.Base
open Tr1Htmlit_Std.Htmlit

let register_before_tests ~andhello_file ~andhelloagain_module
    ~andhelloagain_file () =
  Test.register ~__FILE__ ~title:"focus on what you run" ~tags:[]
  @@ fun () ->
  let ref_cmd, hooks = RunDk.make_capture () in
  let* _dk_output = RunDk.run_dk ~hooks [andhelloagain_module] in
  let open Doc in
  let open El in
  append
    (usection
       [ ucontainer
           [ unsafe_raw
               {section|
        <div class="card block">
            <div class="card-content">
                <p class="title">
                    Focus on what you are running
                </p>
            </div>
        </div>
        |section}
           ; upar
               [ ucodeaction ~ide:(txt "Open") ~browser:None andhelloagain_file
               ; txt "It has the same content as "
               ; ucode andhello_file
               ; txt ".\n"
               ; unsafe_raw "However, <em>it has red squiggly lines</em>." ]
           ; unsafe_raw
               {section|
<p class="block">
        The <span style="color: hsl(349.8, 53.6%, 45%)"><strong>red</strong></span> is an indication that DkCoder has not compiled the script.
        That is a <strong>good thing</strong> because generally you don't want to waste time compiling every script every time you make an edit in a large project.
        </p>|section}
           ; upar
               [ txt "Now run the "
               ; ucode andhelloagain_module
               ; txt " script with:" ]
           ; ucodeblock `Shell !ref_cmd
           ; unsafe_raw
               {section|
<p class="block">Visual Studio Code should remove the red in a minute.</p>
        <div class="message block is-info ml-2">
         <div class="message-header">Faster?</div>
         <div class="message-body">You can speed up how fast Visual Studio Code recognizes the newly focused script by using the <code>View &gt; Command Palette</code> menu, and then using the <code>OCaml: Restart Language Server</code> action.</div>
        </div>
        |section}
           ; upar [txt "DkCoder optimizes for rapid iterative development by:"]
           ; uol
               [ splice
                   [ txt "Only compiling the script that you last ran (ex. "
                   ; ucode andhelloagain_file
                   ; txt ").\n"
                   ; txt
                       "If your script requires other scripts to run, those \
                        are also compiled." ]
               ; splice
                   [ txt
                       "Compiling all the scripts in a project when you first \
                        open Visual Studio Code. "
                   ; txt
                       "That means you can browse your project when you first \
                        start your day without seeing any red. "
                   ; txt
                       "Then, when you have found a script you want to edit or \
                        a new script to add, edit and run that script \
                        repeatedly throughout the day."
                   ; unsafe_raw
                       {section|
<div class="message block is-info is-small ml-2">
  <div class="message-header">Upcoming Changes</div>
  <div class="message-body">Since large projects may have many scripts, a future version of DkCoder will allow you to select which modules are automatically compiled at startup.</div>
</div>
                |section}
                   ] ] ] ] ) ;
  unit
[@@warning "-unused-var-strict"]
