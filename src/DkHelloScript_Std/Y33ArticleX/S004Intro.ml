open Tr1Tezt_C.Tezt
open Tr1Tezt_C.Tezt.Base
open Tr1Htmlit_Std.Htmlit

let register_before_tests ~andhello_module () =
  Test.register ~__FILE__ ~title:"introduction" ~tags:[]
  @@ fun () ->
  let (), code_ =
    [%sample
      let () = Tr1Stdlib_V414Io.StdIo.print_endline "Hello Builder!"

      let eval () = ()]
  in
  let ( let* ) = Lwt.bind in
  let ref_cmd, hooks = RunDk.make_capture () in
  let* dk_output = RunDk.run_dk ~hooks ["DkHelloScript_Std.AndHello"] in
  let open Doc in
  let open El in
  append
    (usection
       [ ucontainer
           [ upar
               [ txt "Hello Builder!\n"
               ; txt
                   "Scripting is a small, free and important piece of DkSDK.\n"
               ]
           ; upar
               [ unsafe_raw
                   "A few clicks from your web browser and four (4) minutes \
                    later you and your Windows and macOS users can start \
                    scripting with <strong>DkCoder</strong>. And all users, \
                    including glibc-based Linux desktop users, can use their \
                    Unix shells or Windows PowerShell. Nothing needs to be \
                    pre-installed on Windows and macOS. Just copy and paste \
                    two lines (you'll see examples soon) and your script is \
                    running and your project is editable with an LSP-capable \
                    IDE like Visual Studio Code." ]
           ; upar
               [ txt
                   "Unlike most scripting frameworks, DkCoder solves the \
                    problem of scale: you start with small scripts that do \
                    immediately useful things for you and your team, and when \
                    inevitably you need to expand, distribute or embed those \
                    scripts to make full-featured applications, you don't need \
                    to throw out what you have already written. "
               ; txt
                   "DkCoder is a re-imagining of the scripting experience that \
                    re-uses the best historical ideas:\n" ]
           ; uol
               [ splice
                   [ txt "You don't write build files."
                   ; unsafe_raw
                       " <em>If that sounds like Unix <code>/bin/sh</code> or \
                        the Windows Command Prompt, that is intentional.</em>"
                   ]
               ; splice
                   [ txt "Most files you write can be immediately run."
                   ; unsafe_raw
                       " <em>If that sounds like how Python scripts are almost \
                        indistinguishable from Python modules, or like \
                        JavaScript modules, that is intentional.</em>" ]
               ; splice
                   [ txt
                       "Most files you write can be referenced with a \
                        fully-qualified name."
                   ; unsafe_raw
                       " <em>If that sounds like Java packages and how that \
                        has been proven to scale to large code bases, that is \
                        intentional.</em>" ]
               ; splice
                   [ txt "Your scripts play well together and don't bit rot."
                   ; unsafe_raw
                       " <em>It is conventional to add static typing \
                        (Typescript, mypy) when scripting projects get large. \
                        DkCoder has type-safety from Day One that is safer and \
                        easier to use.</em>" ] ]
           ; upar
               [ txt
                   "You'll start with the mouse-click install and the basic \
                    one-liner:" ]
           ; ucodeblock `OCaml code_
           ; upar [txt "which you run with a single command:"]
           ; ucodeblock `Shell !ref_cmd
           ; ucodeblock `Output dk_output
           ; upar
               [ txt
                   "You'll do a quick tour of the prior art where we will \
                    acknowledge situations when DkCoder is not the right tool \
                    for you.\n" ]
           ; upar [txt "You'll see three examples of scripts:\n"]
           ; uol
               [ splice
                   [ txt
                       "the integration test script that produces this \
                        documentation page" ]
               ; splice
                   [ txt
                       "a 2D game to show non-traditional uses of scripts and \
                        the re-use of existing code" ]
               ; splice
                   [ txt
                       "the security-conscious production service managing \
                        subscriptions for "
                   ; ulink ~url:"https://diskuv.com/pricing/" "DkSDK Pricing" ]
               ]
           ; upar
               [ txt
                   "Along the way you'll encounter a small language made by a \
                    community who can write some very good libraries.\n"
               ; txt
                   "And a software kit that gets out the way and makes good \
                    software accessible.\n" ]
           ; uinfo
               ~header:[txt "Feedback?"]
               [ txt
                   "If you want to \"like\" DkCoder with GitHub stars, or if \
                    you want help with DkCoder, please visit "
               ; unsafe_raw
                   {|<a href="https://github.com/diskuv/dkcoder">https://github.com/diskuv/dkcoder</a>|}
               ; txt "." ]
           ; upar [txt "Let's begin!\n"] ] ] ) ;
  unit
[@@warning "-unused-var-strict"]
