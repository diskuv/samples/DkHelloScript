open Tr1Tezt_C.Tezt
open Tr1Tezt_C.Tezt.Base
open Tr1Htmlit_Std.Htmlit
module Printf = Tr1Stdlib_V414CRuntime.Printf

let register_before_tests ~article_module ~article_file () =
  Test.register ~__FILE__ ~title:"testing" ~tags:[]
  @@ fun () ->
  let self_FILE = __FILE__ in
  let open Doc in
  let open El in
  append
    (usection
       [ ucontainer
           [ ucard ~title:"Integration Testing" []
           ; upar
               [ txt
                   "Let's run a script that is simultaneously both an \
                    integration test and source of documentation:" ]
           ; ucodeblock `Shell
               (Printf.sprintf "./dk %s %s --serve" Tr1Version.run_module
                  article_module )
           ; upar
               [ txt "While that is running, open your web browser to "
               ; ulink ~url:"http://localhost:8080" "http://localhost:8080" ]
           ; upar
               [ txt
                   "You will see a preview of the documentation you are \
                    reading right now! "
               ; unsafe_raw
                   "<strong>The documentation you are reading is a side-effect \
                    of running integration tests.</strong>" ]
           ; ucodeaction ~ide:(txt "Open") ~browser:(Some "open") article_file
           ; txt "You should see:"
           ; ucodefile article_file
           ; upar
               [ txt "The test infrastructure is provided by the module "
               ; ucode "Tr1Tezt_C.Tezt"
               ; txt ". I recommend you read the "
               ; ulink
                   ~url:
                     "https://research-development.nomadic-labs.com/announcing-tezt.html"
                   "Announcing Tezt by Nomadic Labs"
               ; txt " article to see what it can do and why it was developed. "
               ]
           ; uinfo
               [ txt
                   "DkCoder has Tezt pre-installed, so you won't have to \
                    create 'dune' files or do any 'dune build' steps that \
                    regular OCaml developers would have to do. To use Tezt, \
                    follow the examples that are given to you in this \
                    documentation." ]
           ; upar
               [ txt "I wrote the module "
               ; ucode Doc.__MODULE_ID__
               ; txt
                   " in a very \"scripty\" style: it incrementally builds \
                    documentation in memory. "
               ; txt
                   "It renders to either HTML or Markdown, and makes use of \
                    the "
               ; ulink ~url:"https://bulma.io/" "Bulma CSS Framework"
               ; txt " for styling and layout. "
               ; txt
                   "You can copy and customize the module in your own scripts. "
               ; txt "For those of you familiar with "
               ; ulink
                   ~url:
                     "https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction"
                   "JavaScript and the DOM"
               ; txt
                   ", the incremental approach is similar to how your web \
                    browser renders a web page. For everybody else, just think \
                    of a file being created in-memory the first time "
               ; ucode Doc.__MODULE_ID__
               ; txt
                   " is accessed, and through the use of several helper \
                    functions that write (\"append\") fragments at the end of \
                    the file, the "
               ; ucode
                   (Printf.sprintf "%s.register_before_tests ()"
                      Doc.__MODULE_ID__ )
               ; txt
                   " is able to print a complete documentation file to the \
                    console or a real file." ]
           ; upar
               [ txt "The "
               ; ucode "title_section ()"
               ; txt
                   " function is the first documentation fragment that is \
                    built. "
               ; txt
                   "You should recognize it on the top of this documentation \
                    page." ]
           ; uinfo
               ~header:[txt "Imperative programming"]
               [ txt "You may have heard that OCaml is a functional language. "
               ; txt
                   "However, OCaml also supports imperative programming with \
                    global variables. "
               ; txt
                   "The Tezt test framework uses imperative programming to \
                    allow you to register tests whenever you want. "
               ; txt
                   "I personally find imperative programming the most natural \
                    programming model when writing scripts. " ]
           ; upar
               [ txt
                   "Each section of the documentation has its own test script \
                    which registers its own integration test. " ]
           ; upar
               [ txt
                   "Once all of the tests are registered, they are all run \
                    with the OCaml function call:"
               ; ucodeblock `OCaml "Test.run ()" ]
           ; upar
               [txt "Let's drill down into this section of the documentation. "]
           ; ucodeaction ~ide:(txt "Open") ~browser:(Some "open") self_FILE
           ; ucodefile self_FILE
           ; upar
               [ txt "You will see some brief boilerplate to register the test ("
               ; ucode "Test.register ~__FILE__ ..."
               ; txt
                   "). The test adds a \"section\" fragment to the in-memory \
                    documentation. In fact, the side-effect of creating the \
                    section fragment is all that the test does." ]
           ; upar [txt "Let's see something more complex."]
           ; ucodeaction
               ~ide:(txt "Open the \"Basic shell scripting\" test")
               ~browser:(Some "open") S024BasShell.__FILE__
           ; ucodefile S024BasShell.__FILE__
           ; upar
               [ txt "In this test we run a script from the command line ("
               ; ucode "RunDk.make_capture ()"
               ; txt " and "
               ; ucode "RunDk.run_dk ~hooks [shexpcountdown_module]"
               ; txt "). "
               ; txt
                   "The source code, the script command and its output become \
                    part of the documentation:"
               ; ucodeblock `OCaml
                   {|
; ucodefile shexpcountdown_file
; ucodeblock `Shell !ref_cmd
; ucodeblock `Output dk_output
|}
               ] ] ] ) ;
  unit
[@@warning "-unused-var-strict"]
