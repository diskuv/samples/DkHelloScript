[@@@warning "-unused-open"]

open Tr1Tezt_C.Tezt
open Tr1Tezt_C.Tezt.Base
open Tr1Htmlit_Std.Htmlit
open Tr1Stdlib_V414CRuntime

let register_before_tests () =
  Test.register ~__FILE__ ~title:"making your own script project" ~tags:[]
  @@ fun () ->
  let open Doc in
  let open El in
  append
    (usection
       [ ucontainer
           [ unsafe_raw
               {section|
        <div class="card block">
            <div class="card-content">
                <p class="title">
                    Making Your Own Script Project
                </p>
            </div>
        </div>
        <p class="block">
            <strong>FIRST</strong>, create an empty folder for your project and open it in Visual Studio Code.
            If your project should use Git source control, in the Terminal run:

```sh
git init
```

</p>
        <div class="message is-success ml-3">
            <div class="message-header">Files Created</div>
            <div class="message-body">

```output
.
└─── .git/
```

</div>
        </div>
        <p class="block">
            <strong>SECOND</strong>, in the Terminal paste the following to get four files into your project:

```sh
git clone https://github.com/diskuv/dkcoder.git
dkcoder/dk user.dkml.wrapper.upgrade HERE
./dk dkml.wrapper.upgrade DONE
```

</p>
        <div class="message is-success ml-3">
            <div class="message-header">Files Created</div>
            <div class="message-body">

```output
.
├── .gitattributes
├── __dk.cmake
├── dk
└── dk.cmd
```

</div>
        </div>
        <p class="block">
        <strong>THIRD</strong>, create the file <code>.vscode/extensions.json</code>:

```json
{
  "recommendations": ["ocamllabs.ocaml-platform"]
}
```

</p>
        <div class="message is-success ml-3">
            <div class="message-header">Files Created</div>
            <div class="message-body">

```output
.
└── .vscode/
    └── extensions.json
```

</div>
        </div>
|section}
           ; upar
               [ strong [txt "FOURTH"]
               ; txt ", create the file "
               ; ucode ".vscode/settings.json"
               ; txt ":\n" ]
           ; ucodeblock `Json
               {|
{
  "ocaml.sandbox": {
    "kind": "custom",
    "template": "${firstWorkspaceFolder}/dk DkRun_Project.RunQuiet --log-level ERROR --fixed-length-modules false -- MlStd_Std.Exec -- $prog $args"
  }
}
|}
           ; unsafe_raw
               {section|
<div class="message is-success ml-3">
    <div class="message-header">Files Created</div>
    <div class="message-body">

```output
.
└── .vscode/
    └── settings.json
```

</div>
        </div>
        <p class="block">
            <span class="icon-text">
                <span class="icon has-text-success">
  <img
    alt="Checkmark"
    class="icon-ionic-md-checkmark-circle"
    src="/img/icon-ionic-md-checkmark-circle-10@1x.png"
  />
                </span>
                <span>
                    <strong>DONE!</strong> You can add new scripts now.
                    <em>Yep, you can expect that a future version of DkCoder will have a script to create a new project!</em>
                </span>
            </span>
        </p>
|section}
           ] ] ) ;
  unit
[@@warning "-unused-var-strict"]
