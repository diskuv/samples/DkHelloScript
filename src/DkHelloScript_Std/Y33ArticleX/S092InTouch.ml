[@@@warning "-unused-open"]

open Tr1Tezt_C.Tezt
open Tr1Tezt_C.Tezt.Base
open Tr1Htmlit_Std.Htmlit

let register_before_tests () =
  Test.register ~__FILE__ ~title:"getting in touch" ~tags:[]
  @@ fun () ->
  let open Doc in
  let open El in
  append
    (usection
       [ ucontainer
           [ unsafe_raw
               {section|
        <div class="card block">
            <div class="card-content">
                <p class="title">
                    Getting in touch
                </p>
            </div>
        </div>
        <div class="block">
            I'm a recovering Luddite when it comes to social media; I deactivated Facebook and stopped using
            LinkedIn years ago. That will change. In the meantime, it is best to post OCaml questions on <a
                href="discuss.ocaml.org">discuss.ocaml.org</a>.
            And you can follow or DM me on the low-volume
            <a href="https://twitter.com/diskuv">
                @Diskuv on &#x1D54F;</a>.
        </div>
|section}
           ] ] ) ;
  unit
  [@@warning "-unused-var-strict"]
