open Tr1Tezt_C.Tezt
open Tr1Tezt_C.Tezt.Base
open Tr1Clap_Std
module S = Tr1TinyHttpd_Std.TinyHttpd

let register_before_tests () =
  (* Add [--serve | --no-serve] command line option *)
  let section_httpserver =
    Clap.section "HTTP SERVER"
      ~description:
        "Documentation is collected as a side-effect of exercising the source \
         code (that is, running tests) in this project. You may serve that \
         content to a web browser using one of the options in this section \
         -AND- make sure the 'HTTP server' test runs."
  in
  let serve =
    Clap.flag ~section:section_httpserver
      ~description:"Start a HTTP server that serves the article."
      ~set_long:"serve" ~unset_long:"no-serve" false
  in
  (* Make a test that runs the HTTP server *)
  Test.register ~__FILE__ ~title:"HTTP server" ~tags:["httpd"]
  @@ fun () ->
  (* Only do something useful if [-serve] is enabled. *)
  if serve then (
    (* Create the HTTP server *)
    let server = S.create () in
    (* Tell the HTTP server that the main page should be handled
       by the anonymous function below. *)
    S.add_route_handler ~meth:`GET server
      S.Route.(return)
      (fun _req ->
        S.Response.make_string
          ~headers:[("content-type", "text/html; charset=utf-8")]
          (Ok (Doc.print `Html ())) ) ;
    (* After the HTTP server has run, tell user the URL to access
       the webpage *)
    let after_init () =
      Log.report "Listening on http://%s:%d ... press Ctrl-C to stop"
        (S.addr server) (S.port server)
    in
    (* Run the HTTP server *)
    match S.run ~after_init server with Ok () -> () | Error e -> raise e )
  else () ;
  unit
