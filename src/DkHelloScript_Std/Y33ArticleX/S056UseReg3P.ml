[@@@warning "-unused-open"]

open Tr1Tezt_C.Tezt
open Tr1Tezt_C.Tezt.Base
open Tr1Htmlit_Std.Htmlit

let register_before_tests () =
  Test.register ~__FILE__ ~title:"using registered third-parties" ~tags:[]
  @@ fun () ->
  let open Doc in
  let open El in
  append
    (usection
       [ ucontainer
           [ unsafe_raw
               {section|
        <div class="card block">
            <div class="card-content">
                <p class="title">
                    Using registered third-parties
                </p>
            </div>
        </div>
        <p class="block">
            In the last sections we walked through the "distributed" way of getting third-party scripts.
            That method was perfect when someone wants to share their code and:
        </p>
        <ol class="block">
            <li>They have a mechanism for their fellow developers to discover their URLs (blog posts, etc.)
                and have instructed them how to add it to their <code>dkproject.jsonc</code> project
                configuration.
            </li>
            <li>Their scripts work with just the standard runtime libraries
                (like the <code>Bogue</code> graphics library) and pure OCaml.</li>
            <li>Their scripts are not performance sensitive.
                (Confer with the <u>Advanced: Technical Details on Performance</u> callout below). </li>
        </ol>
        <div class="message is-info">
            <div class="message-body">
                The second way to get third-party scripts is <strong>centralized</strong> fetching.
                DkCoder knows how to do the fetching on your behalf.</div>
        </div>
        <p class="block">Centralized scripts are available from the <strong>DkRegistry</strong>
            and can be used by any DkCoder user.
            You can think of these centralized scripts like mobile applications you can download from an App Store.
            A centralized script is perfect when someone wants to share their code and wants any of the following:
        </p>
        <ol class="block">
            <li>Their scripts to be auto-imported whenever a DkCoder user accesses a centralized package
                inside script code or from the <code>./dk DkRun_V0_4.Run</code> command line.
                <em>No mucking around with <code>dkproject.jsonc</code>.</em>
            </li>
            <li>Their scripts to be distributed as "native compiled" binaries for speed.
                (Confer with the <u>Advanced: Technical Details on Performance</u> callout below).
            </li>
            <li>Their scripts to access C, Java and other programming languages.</li>
            <li>Their scripts to be distributed with binary assets.</li>
            <li>Their users to know their scripts don't suffer from bitrot.
                <em>It really sucks to deliver a product for your users and then have your script not work
                    because someone else's script no longer works.
                </em>
            </li>
        </ol>
        <div class="message is-dark">
            <div class="message-header">Advanced: Technical Details on Performance</div>
            <div class="message-body">Distributed scripts are run in a "bytecode" mode.
                Bytecode is a layer of abstraction used by many programming languages
                like Python, JavaScript and OCaml that makes scripts portable at the cost of speed.
                However, the speed slowdown can be solved by using highly optimized, pre-compiled runtime libraries.
                For example, Python is famous for its optimized <code>numpy</code> library for number crunching.
                In DkCoder the "centralized" third-party scripts are pre-compiled to
                machine code <em>and</em> can use pre-compiled libraries from other languages.
                All of that to say: <strong>DkSDK centralized scripts are fast.</strong>
            </div>
        </div>
|section}
           ] ] ) ;
  unit
  [@@warning "-unused-var-strict"]
