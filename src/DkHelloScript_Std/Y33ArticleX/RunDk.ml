(** Run ["./dk"] from within tests. *)

open Tr1Tezt_C.Tezt
open Tr1Tezt_C.Tezt.Base
open Tr1Stdlib_V414Base
open Tr1Stdlib_V414CRuntime

let run_dk ?env ?expect_failure ~hooks args =
  (* Set environment *)
  let env = match env with None -> String_map.empty | Some env -> env in
  let env =
    (* Set DKRUN_ENV_URL_BASE environment variable for [./dk DkRun_Env.Run] *)
    match Tr1Version.run_env_url_base with
    | None ->
        env
    | Some base_url ->
        String_map.add "DKRUN_ENV_URL_BASE" base_url env
  in
  (* Our [./dk] instructions are for Unix shells and PowerShell, but not
     Command Prompt. We will get "Exec format error" on Command Prompt
     if we don't use [dk.cmd]. *)
  let dk = if Sys.win32 then ".\\dk.cmd" else "./dk" in
  (* The [dune] generator output does not interfere with the build files
     in '#s/' of the default [dune-ide] generator. *)
  ProcessShexp.run_and_read_stdout ~hooks ?expect_failure ~env dk
    (Tr1Version.run_module :: "--generator=dune" :: "--" :: args)

let make_capture () : string ref * Process_hooks.t =
  let ref_cmd = ref "" in
  let on_log _s = () in
  let on_spawn cmd args =
    let rec aux = function
      | cmd', args' when String.ends_with ~suffix:"\\dk.cmd" cmd' ->
          (* Switch to Unix/PowerShell syntax. Hide temp. directory *)
          aux ("./dk", args')
      | cmd', first :: "--generator=dune" :: rest ->
          (* Remove [--generator=dune] from output *)
          aux (cmd', first :: rest)
      | cmd', args' ->
          (cmd', args')
    in
    ref_cmd :=
      let cmd', args' = aux (cmd, args) in
      String.concat " " (cmd' :: args')
  in
  (ref_cmd, {on_log; on_spawn})
