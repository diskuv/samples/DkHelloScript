(** [Doc] is a document module that may be upstreamed into DkCoder
    or a Them library.
    
    It can render to HTML and to Markdown.
    
    {1 Markdown}

    Many Markdown standards allow switching from pure Markdown
    into raw HTML mode. That is,

    {v

    This line has a Markdown code phrase `let () = ()`.

    <div>But using an HTML tag like "div" we have now switched to raw HTML.
    `let () = ()` will not render as a code phrase!</div>
    v}

    When rendering Markdown, [Doc] handles when Markdown switches
    from Markdown mode back and forth to raw HTML mode. When in
    raw HTML mode the [Doc] Markdown rendering will produce code
    phrases as ["<code>...</code>"] rather than ["`...`"].
    
    Many static site generators (SSG) like Gatsby can do processing
    only when the content is in Markdown mode. For example, the
    source code blocks need to be rendered as fenced
    code blocks (["```"]) rather than raw HTML ["<pre><code>"].

    You may lose syntax highlighting and anything else the SSG gives you
    if you use raw HTML mode.

    You can temporarily switch to raw HTML mode with {!uhtmlblock}. *)

open Tr1Htmlit_Std.Htmlit

val append : El.html -> unit
(** [append html] adds the HTML [html] to the last child of the current
    content.  *)

(** {1 Layout} *)

val usection : El.html list -> El.html
(** [usection items] is a division of a page or an article that contains
    items [items]. *)

val ucontainer : El.html list -> El.html
(** [ucontainer items] is a container for items [items] that, conventionally,
    centers each item horizontally. *)

val ucard : title:string -> El.html list -> El.html
(** [ucard ~title items] is a card containing items [items] with the title
    [title]. *)

(** {1 Blocks} *)

val upar : El.html list -> El.html
(** [upar items] is a paragraph of items [items]. *)

val uinfo : ?header:El.html list -> El.html list -> El.html
(** [uinfo ?header body] is an informational message with the optional
    message header [header] and the message body [body]. *)

val uol : El.html list -> El.html
(** [uol items] is an ordered list of items [items]. *)

val uul : El.html list -> El.html
(** [uul items] is an unordered list of items [items]. *)

val ucodeblock :
  [< `OCaml | `Output | `Json | `Shell | `Text] -> string -> El.html
(** [ucodeblock language code] is a source code block containing the source
    code [code] written in the language [language].
    
    The first line is removed if it is blank.
    The last line is removed if it is blank. 
                
    In HTML the code blocks use:
    
    {v
    <pre><code class="language-LANGUAGE">...</code></pre>
    v}

    In Markdown the code blocks use:

    {v

    ```LANGUAGE
    ...
    ```

    v}
    *)

val ucodeaction : ide:El.html -> browser:string option -> string -> El.html
(** [ucodeaction ~ide ~browser SomeModule.__FILE__] describes an action done
    to the location of the source code for [SomeModule].
    
    [ide] is the action within an IDE.
    
    [browser] is the action in a web browser.
    
    Example for
    [ucodeaction ~ide:(El.txt "Open") ~browser:(Some "open") AndHello.__FILE__]
    rendered in Markdown:

    {v
    Open `src/DkHelloScript_Std/AndHello.ml` in your IDE or open
    [src/DkHelloScript_Std/AndHello.ml](https://.../src/DkHelloScript_Std/AndHello.ml)
    in your web browser.
    v}

    The same example
    [ucodeaction ~ide:(El.txt "Open") ~browser:None AndHello.__FILE__]
    without the browser:

    {v
    Open `src/DkHelloScript_Std/AndHello.ml` in your IDE.
    v}
    *)

val uhtmlblock : (unit -> El.html) -> El.html
(** [uhtmlblock f] switches into raw HTML mode for the duration of the
    function [f]. *)

(** {1 Inline} *)

val ucode : string -> El.html
(** [ucode code] is a source code phrase or word [code].
    
    In HTML the code renders as ["<code>...</code>"].
    
    In Markdown the code renders as ["`...`"]. *)

val ucodefile : string -> El.html
(** [ucodefile SomeModule.__FILE__] is a source code block containing
        the source code of the module [SomeModule].
    
        Currently the source code language is assumed to be OCaml. *)

val ulink : url:string -> string -> El.html
(** [ulink ~url content] is a hyperlink to the URL [url] with content
    [content]. *)

(** {1 Actions} *)

val print : [`Html | `Markdown] -> unit -> string
(** [print format ()] prints the {!append}-ed content to a string in the
    documentation format [format]. *)

val register_before_tests : unit -> unit
(** [register_before_tests ()] must be called to generate documentation. *)

(** {1 Introspection} *)

val __FILE__ : string
val __MODULE_ID__ : string