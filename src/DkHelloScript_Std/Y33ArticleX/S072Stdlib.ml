[@@@warning "-unused-open"]

open Tr1Tezt_C.Tezt
open Tr1Tezt_C.Tezt.Base
open Tr1Htmlit_Std.Htmlit

let register_before_tests () =
  Test.register ~__FILE__ ~title:"stdlib" ~tags:[]
  @@ fun () ->
  let open Doc in
  let open El in
  append
    (usection
       [ ucontainer
           [ unsafe_raw
               {section|
        <div class="card block">
            <div class="card-content">
                <p class="title">
                    Standard OCaml Library - Stdlib
                </p>
            </div>
        </div>

The `Stdlib` standard library provides the basic operations over the built-in
types (numbers, booleans, byte sequences, strings, exceptions, references,
lists, arrays, input-output channels, ...) and standard library modules.

In conventional OCaml programs the `Stdlib` is automatically opened. That means
you can type `print_endline "Hi"` rather than `Stdlib.print_endline "Hi"`.
However, in DkCoder access to the `Stdlib` is restricted: `Stdlib` does a bit
_too much_. In particular, input-output channels and threading do not make sense
when compiling to JavaScript for use on a web page. Even more critical is that
the `Stdlib.Obj` module is included, which has unsafe functions that make it
impossible to prove overall code safety.

You can explicitly get back the unsafe standard library by performing an open at
the top of your scripts:

```ocaml
open Tr1Stdlib_V414
```

However, it is recommended to open the pieces of the standard library you
actually need. That way if, for example, your script does not use files it can
be compiled to JavaScript.

<table class="table">
            <caption class="subtitle">Splitting the OCaml Standard Library</caption>
            <thead>
                <tr>
                    <th>Package</th>
                    <th>Description</th>
                    <th>Modules</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Package</th>
                    <th>Description</th>
                    <th>Modules</th>
                </tr>
            </tfoot>
            <tbody>
                <tr>
                    <th><code>Tr1Stdlib_V414CRuntime</code></th>
                    <td>Modules that need a C99 runtime</td>
                    <td><code>Arg, Callback, Filename, Format, In_channel, LargeFile, Lexing, Out_channel, Printexc, Printf, Scanf, StdExit<sup><strong>1</strong></sup>, Sys, Unix, UnixLabels</code></td>
                </tr>
                <tr>
                    <th><code>Tr1Stdlib_V414Io</code></th>
                    <td>Modules for input and output. Can't be used on Android and iOS.</td>
                    <td><code>StdIo<sup><strong>2</strong></sup></code></td>
                </tr>
                <tr>
                    <th><code>Tr1Stdlib_V414Threads</code></th>
                    <td>Modules to create and coordinate threads. Brings in <code>Tr1Stdlib_V414CRuntime</code>.</td>
                    <td><code>Condition, Event, Thread</code></td>
                </tr>
                <tr>
                    <th><code>Tr1Stdlib_V414Gc</code></th>
                    <td>Modules for garbage collection</td>
                    <td><code>Ephemeron, Gc, Weak</code></td>
                </tr>
                <tr>
                    <th><code>Tr1Stdlib_V414Random</code></th>
                    <td>Modules for random numbers</td>
                    <td><code>Random</code></td>
                </tr>
                <tr>
                    <th><code>Tr1Stdlib_V414Unsafe</code></th>
                    <td>Modules that can break type-safety</td>
                    <td><code>Dynlink, Marshal, Obj</code></td>
                </tr>
                <tr>
                    <th><code>Tr1Stdlib_V414Base</code></th>
                    <td>Modules safe for use on any platform. Also includes safe functions like <code>sqrt</code>.</td>
                    <td><code>Array, ArrayLabels, Atomic, Bool, Buffer, Bytes, BytesLabels, Char, Complex, Digest, Either, Float, Fun, Hashtbl, Int, Int32, Int64, Lazy, List, ListLabels, Map, MoreLabels, Nativeint, Oo, Option, Parsing, Queue, Result, Seq, Set, Stack, StdBinds<sup><strong>3</strong></sup>, StdLabels, String, StringLabels, Uchar, Unit</code></td>
                </tr>
            </tbody>
        </table>
        <div class="message is-info ml-3">
            <div class="message-header">Deprecated Stdlib modules</div>
            <div class="message-body">The <code>ThreadUnix</code>, <code>Stream</code>, <code>Pervasives</code>, and <code>Genlex</code> modules have been deprecated in OCaml and do not appear in any of the <code>Tr1Stdlib_V414*</code> packages</div>
        </div>
        <div class="message is-info ml-3">
            <div class="message-header">Footnote 1: StdExit</div>
            <div class="message-body">StdExit is not a module provided by Stdlib. It is a new module that contains all the program termination types and functions of Stdlib:

```ocaml
val exit : int -> 'a
(** Terminate the process, returning the given status code
   to the operating system: usually 0 to indicate no errors,
   and a small positive integer to indicate failure.
   ... *)

val at_exit : (unit -> unit) -> unit
(** Register the given function to be called at program termination
   time. ... *)
```

Full documentation is at
[Stdlib - Program termination](https://v2.ocaml.org/releases/4.14/api/Stdlib.html#1_Programtermination)

</div>
        </div>
        <div class="message is-info ml-3">
            <div class="message-header">Footnote 2: StdIo</div>
            <div class="message-body">StdIo is not a module provided by Stdlib. It is a new module that contains all the input/output types and functions of Stdlib:

```ocaml
type in_channel
(** The type of input channel. *)

type out_channel
(** The type of output channel. *)

val stdin : in_channel
(** The standard input for the process. *)

val stdout : out_channel
(** The standard output for the process. *)

val stderr : out_channel
(** The standard error output for the process. *)

val print_char : char -> unit
(** Print a character on standard output. *)

val print_string : string -> unit
(** Print a string on standard output. *)

val print_bytes : bytes -> unit
(** Print a byte sequence on standard output. *)

val print_int : int -> unit
(** Print an integer, in decimal, on standard output. *)

val print_float : float -> unit
(** Print a floating-point number, in decimal, on standard output. *)

val print_endline : string -> unit
(** Print a string, followed by a newline character, on
   standard output and flush standard output. *)

val print_newline : unit -> unit
(** Print a newline character on standard output, and flush
   standard output. *)

val prerr_char : char -> unit
(** Print a character on standard error. *)

val prerr_string : string -> unit
(** Print a string on standard error. *)

val prerr_bytes : bytes -> unit
(** Print a byte sequence on standard error. *)

val prerr_int : int -> unit
(** Print an integer, in decimal, on standard error. *)

val prerr_float : float -> unit
(** Print a floating-point number, in decimal, on standard error. *)

val prerr_endline : string -> unit
(** Print a string, followed by a newline character on standard
   error and flush standard error. *)

val prerr_newline : unit -> unit
(** Print a newline character on standard error, and flush
   standard error. *)

val read_line : unit -> string
(** Flush standard output, then read characters from standard input
   until a newline character is encountered. ... *)

val read_int_opt: unit -> int option
(** Flush standard output, then read one line from standard input
   and convert it to an integer. ... *)

val read_int : unit -> int
(** Same as {!read_int_opt}, but raise [Failure "int_of_string"]
   instead of returning [None]. *)

val read_float_opt: unit -> float option
(** Flush standard output, then read one line from standard input
   and convert it to a floating-point number. ... *)

val read_float : unit -> float
(** Same as {!read_float_opt}, but raise [Failure "float_of_string"]
   instead of returning [None]. *)

type open_flag =
    Open_rdonly      (** open for reading. *)
  | Open_wronly      (** open for writing. *)
  | Open_append      (** open for appending: always write at end of file. *)
  | Open_creat       (** create the file if it does not exist. *)
  | Open_trunc       (** empty the file if it already exists. *)
  | Open_excl        (** fail if Open_creat and the file already exists. *)
  | Open_binary      (** open in binary mode (no conversion). *)
  | Open_text        (** open in text mode (may perform conversions). *)
  | Open_nonblock    (** open in non-blocking mode. *)
(** Opening modes for {!open_out_gen} and {!open_in_gen}. *)

val open_out : string -> out_channel
(** Open the named file for writing, and return a new output channel
   on that file, positioned at the beginning of the file. ... *)

val open_out_bin : string -> out_channel
(** Same as {!open_out}, but the file is opened in binary mode,
   so that no translation takes place during writes. ... *)

val open_out_gen : open_flag list -> int -> string -> out_channel
(** [open_out_gen mode perm filename] opens the named file for writing,
   as described above. ... *)

val flush : out_channel -> unit
(** Flush the buffer associated with the given output channel,
   performing all pending writes on that channel. ... *)

val flush_all : unit -> unit
(** Flush all open output channels; ignore errors. *)

val output_char : out_channel -> char -> unit
(** Write the character on the given output channel. *)

val output_string : out_channel -> string -> unit
(** Write the string on the given output channel. *)

val output_bytes : out_channel -> bytes -> unit
(** Write the byte sequence on the given output channel. *)

val output : out_channel -> bytes -> int -> int -> unit
(** [output oc buf pos len] writes [len] characters from byte sequence [buf],
   starting at offset [pos], to the given output channel [oc]. ... *)

val output_substring : out_channel -> string -> int -> int -> unit
(** Same as [output] but take a string as argument instead of
   a byte sequence. *)

val output_byte : out_channel -> int -> unit
(** Write one 8-bit integer (as the single character with that code)
   on the given output channel. ... *)

val output_binary_int : out_channel -> int -> unit
(** Write one integer in binary format (4 bytes, big-endian)
   on the given output channel. ... *)

val output_value : out_channel -> 'a -> unit
(** Write the representation of a structured value of any type
   to a channel. ... *)

val seek_out : out_channel -> int -> unit
(** [seek_out chan pos] sets the current writing position to [pos]
   for channel [chan]. ... *)

val pos_out : out_channel -> int
(** Return the current writing position for the given channel. ... *)

val out_channel_length : out_channel -> int
(** Return the size (number of characters) of the regular file
   on which the given channel is opened. ... *)

val close_out : out_channel -> unit
(** Close the given channel, flushing all buffered write operations. ... *)

val close_out_noerr : out_channel -> unit
(** Same as [close_out], but ignore all errors. *)

val set_binary_mode_out : out_channel -> bool -> unit
(** [set_binary_mode_out oc true] sets the channel [oc] to binary
   mode: no translations take place during output. ... *)

val open_in : string -> in_channel
(** Open the named file for reading, and return a new input channel
   on that file, positioned at the beginning of the file. *)

val open_in_bin : string -> in_channel
(** Same as {!open_in}, but the file is opened in binary mode,
   so that no translation takes place during reads. ... *)

val open_in_gen : open_flag list -> int -> string -> in_channel
(** [open_in_gen mode perm filename] opens the named file for reading,
   as described above. ... *)

val input_char : in_channel -> char
(** Read one character from the given input channel. ... *)

val input_line : in_channel -> string
(** Read characters from the given input channel, until a
   newline character is encountered. ... *)

val input : in_channel -> bytes -> int -> int -> int
(** [input ic buf pos len] reads up to [len] characters from
   the given channel [ic], storing them in byte sequence [buf], starting at
   character number [pos]. ... *)

val really_input : in_channel -> bytes -> int -> int -> unit
(** [really_input ic buf pos len] reads [len] characters from channel [ic],
   storing them in byte sequence [buf], starting at character number [pos].
   ... *)

val really_input_string : in_channel -> int -> string
(** [really_input_string ic len] reads [len] characters from channel [ic]
   and returns them in a new string. ...*)

val input_byte : in_channel -> int
(** Same as {!input_char}, but return the 8-bit integer representing
   the character. ... *)

val input_binary_int : in_channel -> int
(** Read an integer encoded in binary format (4 bytes, big-endian)
   from the given input channel. ... *)

val input_value : in_channel -> 'a
(** Read the representation of a structured value, as produced
   by {!output_value}, and return the corresponding value. ... *)

val seek_in : in_channel -> int -> unit
(** [seek_in chan pos] sets the current reading position to [pos]
   for channel [chan]. ... *)

val pos_in : in_channel -> int
(** Return the current reading position for the given channel. ... *)

val in_channel_length : in_channel -> int
(** Return the size (number of characters) of the regular file
    on which the given channel is opened. ... *)

val close_in : in_channel -> unit
(** Close the given channel. ... *)

val close_in_noerr : in_channel -> unit
(** Same as [close_in], but ignore all errors. *)

val set_binary_mode_in : in_channel -> bool -> unit
(** [set_binary_mode_in ic true] sets the channel [ic] to binary
   mode: no translations take place during input. ... *)

val __LOC__ : string
(** [__LOC__] returns the location at which this expression appears in
    the file currently being parsed by the compiler, with the standard
    error format of OCaml: "File %S, line %d, characters %d-%d". *)

val __FILE__ : string
(** [__FILE__] returns the name of the file currently being
    parsed by the compiler. *)

val __LOC_OF__ : 'a -> string * 'a
(** [__LOC_OF__ expr] returns a pair [(loc, expr)] where [loc] is the
    location of [expr] in the file currently being parsed by the
    compiler, with the standard error format of OCaml: "File %S, line
    %d, characters %d-%d". *)
```

Full documentation is at
[Stdlib - Input/output](https://v2.ocaml.org/releases/4.14/api/Stdlib.html#1_Inputoutput)

</div>
        <div class="message is-info ml-3">
            <div class="message-header">Footnote 3: StdBinds</div>
            <div class="message-body">StdBinds is not a module provided by Stdlib. It contains modules that can be opened and used with the ocaml-monadic PPX macros:

```ocaml
(** Open to use [let%bind] with {!Result}. *)
module BindsResult = struct
  exception ResultFailed of string option

  let bind = Result.bind
  let map = Result.map
  let return = Result.ok
  let zero ?msg () = raise (ResultFailed msg)
end

(** Open to use [let%bind] with {!Option}. *)
module BindsOption = struct
  let bind = Stdlib.Option.bind
  let map = Stdlib.Option.map
  let return = Stdlib.Option.some
  let zero () = None
end
```

The macro documentation is at
[ocaml-monadic extensions](https://github.com/zepalmer/ocaml-monadic?tab=readme-ov-file#extensions)

</div>
        </div>
      </div>
|section}
           ] ] ) ;
  unit
  [@@warning "-unused-var-strict"]
