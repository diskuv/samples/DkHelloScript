[@@@warning "-unused-open"]

open Tr1Tezt_C.Tezt
open Tr1Tezt_C.Tezt.Base
open Tr1Htmlit_Std.Htmlit

let register_before_tests () =
  Test.register ~__FILE__ ~title:"runtime requirements" ~tags:[]
  @@ fun () ->
  let open Doc in
  let open El in
  append
    (usection
       [ ucontainer
           [ unsafe_raw
               {section|
        <div class="card block">
            <div class="card-content">
                <p class="title">
                    Runtime requirements
                </p>
            </div>
        </div>

- Windows 10 (version 1903 aka 19H1, or later), or Windows 11, either 32-bit or
  64-bit.
- macOS AMD/Intel or Apple Silicon (Big Sur 11.0 or higher).
- Linux AMD/Intel 64-bit with glibc 2.27 or higher.

  - You will need the following system tools installed:
    - `tar`, `wget` or `curl`, `git`, `unzip`
    - `libssl-dev` (Debian, Ubunutu) or `openssl-devel` (RedHat)
    - `ninja` (this requirement will be removed)
    - If any are missing you will be asked if DkCoder can install them the first time you run `./dk` from the command line.

  - For graphics you will need one of:
    - X11 with a direct OpenGL driver.
      - A good prereq test is running `glxgears` (ex.
        `yum install glx-utils; glxgears`). If that works, DkCoder graphics should
        work.
      - Don't expect the `LIBGL_ALWAYS_INDIRECT=1` environment variable to work.
        That is sometimes recommended when forwarding X11 with `ssh -X` or into a
        Docker container, but it forces the protocol to be a
        [too-old OpenGL version 1.4](https://unix.stackexchange.com/questions/1437/what-does-libgl-always-indirect-1-actually-do).
    - Wayland. Set the environment variable `SDL_VIDEODRIVER=wayland` to force it.
    - Linux Direct Rendering Manager. If supported, you will have a
      `/dev/dri/card0`. Set the environment variable `SDL_VIDEODRIVER=kmsdrm` to
      force it.
  - That's it. Linux desktops are complex!
|section}
           ] ] ) ;
  unit
  [@@warning "-unused-var-strict"]
