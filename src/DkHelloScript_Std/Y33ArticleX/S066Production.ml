open Tr1Tezt_C.Tezt
open Tr1Tezt_C.Tezt.Base
open Tr1Htmlit_Std.Htmlit
module Printf = Tr1Stdlib_V414CRuntime.Printf

let register_before_tests () =
  Test.register ~__FILE__ ~title:"production" ~tags:[]
  @@ fun () ->
  let open Doc in
  let open El in
  append
    (usection
       [ ucontainer
           [ ucard ~title:"Using scripts in Production" []
           ; upar
               [ txt
                   "We've gone through the mechanics of creating your own \
                    projects and seeing what scripts can do on a desktop. "
               ; txt "However, scripts are not limited to running the desktop!"
               ]
           ; upar [txt "Let's run a real production script:"]
           ; ucodeblock `Shell
               {|
git clone --branch V0_3 https://gitlab.com/diskuv/samples/devops/DkSubscribeWebhook.git

./DkSubscribeWebhook/dk DkRun_V0_3.Run -- DkSubscribeWebhook_Std.Subscriptions --help
|}
           ; upar
               [ txt
                   "You'll be seeing the numerous help and options available \
                    to run the production "
               ; unsafe_raw "<em>webhook</em>"
               ; txt " that manages the DkSDK subscriptions at "
               ; ulink ~url:"https://diskuv.com/pricing/" "DkSDK Pricing"
               ; txt "." ]
           ; upar
               [ txt "A "
               ; unsafe_raw "<em>webhook</em>"
               ; txt
                   " is a production microservice that responds to Internet \
                    requests from third parties. "
               ; txt
                   "You could run the webhook yourself to manage your own \
                    customer subscriptions after you have configured some \
                    cloud SaaS services (more on this soon). The basic flow \
                    is:" ]
           ; uol
               [ splice
                   [ txt
                       "Stripe is the payments provider for DkSDK. Stripe \
                        sends an \
                        [invoice.paid](https://docs.stripe.com/invoicing/overview) \
                        event to the webhook after establishing a subscription \
                        from the <https://diskuv.com/pricing> website." ]
               ; splice
                   [ txt
                       "GitLab is the source control provider for DkSDK. A \
                        GitLab [group token is \
                        created](https://docs.gitlab.com/ee/api/group_access_tokens.html#create-a-group-access-token) \
                        for the subscriber that expires after the subscription \
                        (plus a grace period)." ]
               ; splice
                   [ txt
                       "AWS SES is one of the email gateways used by DkSDK. An \
                        AWS SES [email is \
                        sent](https://docs.aws.amazon.com/ses/latest/dg/send-email.html) \
                        to the subscriber containing the group token." ] ]
           ; upar
               [ txt
                   "You could run the webhook script just like you did above. \
                    However, it is common to package up your scripts into a \
                    Docker container for deployment to production. "
               ; txt "Then in production when you execute:" ]
           ; ucodeblock `Shell "docker-compose up --build"
           ; upar
               [ txt
                   "the 100MB webhook container image starts up with all the \
                    command line options and all the credentials to your cloud \
                    SaaS services. The Docker Compose example we provide also \
                    has a Let's Encrypt web interface so that you can manage \
                    SSL certificates for the webhook." ]
           ; upar
               [ txt "I recommend you read the README at "
               ; ulink
                   ~url:
                     "https://gitlab.com/diskuv/samples/devops/DkSubscribeWebhook.git"
                   "https://gitlab.com/diskuv/samples/devops/DkSubscribeWebhook.git"
               ; txt
                   " if you would like to see the production scripts in detail."
               ]
           ; upar
               [ txt
                   "What may not be obvious from looking at the whole \
                    DkSubscribeWebhook project is how it was developed. "
               ; txt "I encourage you to look at the "
               ; ulink
                   ~url:
                     "https://gitlab.com/diskuv/samples/devops/DkSubscribeWebhook/-/commits/V0_3?ref_type=heads"
                   "git commit history"
               ; txt
                   " to see how each SaaS provider was tested and developed \
                    separately as runnable scripts. "
               ; txt
                   "Those same \"provider\" scripts can be used for manually \
                    administering subscriptions. "
               ; unsafe_raw
                   "<strong>They don't bitrot</strong> and they compose well \
                    into the larger webhook service." ] ] ] ) ;
  unit
[@@warning "-unused-var-strict"]
