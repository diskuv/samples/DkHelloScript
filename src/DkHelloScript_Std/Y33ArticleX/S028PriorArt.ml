[@@@warning "-unused-open"]

open Tr1Tezt_C.Tezt
open Tr1Tezt_C.Tezt.Base
open Tr1Htmlit_Std.Htmlit

let register_before_tests () =
  Test.register ~__FILE__ ~title:"prior art" ~tags:[]
  @@ fun () ->
  let open Doc in
  let open El in
  append
    (usection
       [ ucontainer
           [ unsafe_raw
               {section|
        <div class="card block">
            <div class="card-content">
                <p class="title">
                    Prior Art
                </p>
            </div>
        </div>

This is only a quick tour of the most popular alternatives to DkCoder. Most of
the advantages for DkCoder boil down to _it works on Windows and Unix without
pre-installing other packages_.

- **Shell scripts** like `/bin/bash`. A POSIX-compatible shell script is
  pre-installed on almost all Unix distributions. By comparison, DkCoder has a
  POSIX shell script launcher that transparently installs pre-compiled binaries
  for macOS and the major Linux desktop distributions. However, DkCoder has
  expanded reach with its Windows shell script launcher that transparently
  installs on Windows (and Unix) machines.
- **PowerShell**. The scripts are written in a full-featured programming
  language, and in that respect PowerShell is similar to DkCoder. Furthermore,
  PowerShell has an command shell that is a complete alternative to both the
  Command Prompt on Windows and `/bin/sh` on Unix. DkCoder does not have a
  command shell. However, DkCoder scripts work on both Windows and Unix without
  pre-installing the PowerShell distribution, so DkCoder may be easier to adopt.
- **Perl**. `CPAN` gives Perl a huge standardized package registry with
  standardized package tools. By comparison, DkCoder has no packages (today!) in
  its package registry. However, DkCoder will soon have an alternative to
  package registries that is expected to be the main form of sharing until
  DkCoder is out of its infancy stage. Also, as with shell scripts, Perl is
  pre-installed on almost all Unix distributions. However, DkCoder has expanded
  reach with its Windows and Unix shell script launcher.
- **Python**. Most of the advantages and disadvantages for Perl apply to Python.
  Python is less ubiquitous but more popular than Perl. DkCoder scripting should
  have a similar learning curve to Python, as evidenced by its early use by
  high-schoolers.
- **Fortran**. The DkCoder author (me!) has never written a line of Fortran, but
  its "dynamic dependency" system of Fortran files containing declarations of
  which Fortran modules are used is similar to DkCoder. DkCoder goes a step
  further and drops the need to declare dependencies that are available in the
  DkRegistry.
- etc.

There are a few reasons **not** to use DkCoder:

- OCaml has memory inefficiences due to garbage collection, boxing, cache non-locality and
  wide native types. This would affect you if your application is memory-constrained rather
  than CPU or IO-constrained. The main mitigation is that using OCaml FFI to interface with memory
  efficient languages like C and Rust is not overly complex.
- The OCaml ecosystem is small and biased towards 64-bit Linux applications. This would
  affect you if you develop applications on Windows or for mobile devices.
  The main mitigation is to use cross-platform distributions like `esy`, DkML and DkSDK.
- The company behind DkCoder is very small (at times only one FTE). In particular, some
  features like fast native compilation are going to take substantial time to deliver.  
  The main mitigation is that DkSDK customers get source code rights for life, and can prioritize
  their wishlist with sponsored feature development.
- DkCoder is currently in Alpha. Major features like auto-downloading of other scripts are
  almost feature complete but paused while we collect Alpha feedback. The main
  mitigation is to wait if you need post-Alpha features.
- The licensing of OCaml (LGPL) and the broader OCaml ecosystem (many GPL) may be too restrictive
  for some companies to adopt, especially in comparison to many modern (Rust, Go)
  or dominant (C, Python, JavaScript) ecosystems.
  The main mitigation is to strategically choose either static and shared linking
  (tools in DkSDK CMake and DkSDK FFI C make this easier).
|section}
           ] ] ) ;
  unit
  [@@warning "-unused-var-strict"]
