[@@@warning "-unused-open"]

open Tr1Tezt_C.Tezt
open Tr1Tezt_C.Tezt.Base
open Tr1Htmlit_Std.Htmlit

let register_before_tests () =
  Test.register ~__FILE__ ~title:"using listed third-parties" ~tags:[]
  @@ fun () ->
  let open Doc in
  let open El in
  append
    (usection
       [ ucontainer
           [ unsafe_raw
               {section|
        <div class="card block">
            <div class="card-content">
                <p class="title">
                    Using listed third-parties
                </p>
            </div>
        </div>
        <p class="block">
            We can customize the behavior of the <code>SanetteBogueDemo_Std</code> demo from the last section.
            Open the <code>src/DkHelloScript_Std/Example201.ml</code> file inside Visual Studio Code.
            You should see:
        </p>

```ocaml
let () = SanetteBogueDemo_Std.Demo.demo
    ~fable_title:"A Modest Proposal"
    ~fable_ref:"Dr. Jonathan Swift, 1729"
    ~fable:
{|
For preventing the children of poor people in Ireland,
from being a burden on their parents or country,
and for making them beneficial to the publick.

It is a melancholy object to those, who walk through this great town,
or travel in the country, when they see the streets, the roads, and
...
|}
    ()
```

<p class="block">Run it with ...</p>

```sh
./dk DkRun_V0_4.Run DkHelloScript_Std.Example201
```

<p class="block">... and you will see the text of the demo has changed.</p>
|section}
           ] ] ) ;
  unit
  [@@warning "-unused-var-strict"]
