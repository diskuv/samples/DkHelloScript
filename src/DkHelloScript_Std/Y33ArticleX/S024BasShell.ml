open Tr1Tezt_C.Tezt
open Tr1Tezt_C.Tezt.Base
open Tr1Htmlit_Std.Htmlit

let register_before_tests ~shexpcountdown_module ~shexpcountdown_file () =
  Test.register ~__FILE__ ~title:"basic shell scripting" ~tags:[]
  @@ fun () ->
  let ref_cmd, hooks = RunDk.make_capture () in
  let* dk_output = RunDk.run_dk ~hooks [shexpcountdown_module] in
  let open Doc in
  let open El in
  append
    (usection
       [ ucontainer
           [ ucard ~title:"Basic shell scripting" []
           ; upar
               [ unsafe_raw
                   "In an earlier section an <strong>OCaml</strong> \
                    programming language environment was transparently \
                    downloaded for you. "
               ; txt
                   "You will be using OCaml in this walkthrough, even though \
                    you may find other DkSDK documentation that uses DkCoder \
                    with C and Java. "
               ; txt
                   "You can proceed through this walkthrough without knowing \
                    OCaml." ]
           ; uinfo
               [ unsafe_raw
                   {|Once you are done the DkCoder walkthrough you may want to go to the <a href="https://ocaml.org/docs">OCaml - Learn</a> site. |}
               ; unsafe_raw
                   {|For this walkthrough <strong>we'll stick to ordinary OCaml open-source examples</strong> that you can replicate in a conventional OCaml environment.|}
               ]
           ; upar
               [ txt "In this section we'll be using the "
               ; ulink ~url:"https://github.com/janestreet/shexp" "shexp"
               ; txt " library that was created by "
               ; ulink
                   ~url:"https://www.janestreet.com/join-jane-street/overview/"
                   "Jane Street Capital"
               ; txt
                   ". If you are familiar with traditional shell scripts like "
               ; ucode "bash"
               ; txt " you'll find "
               ; ucode "shexp"
               ; txt " more powerful." ]
           ; upar
               [ ucodeaction ~ide:(txt "Open") ~browser:(Some "open")
                   shexpcountdown_file
               ; txt "You should see:" ]
           ; ucodefile shexpcountdown_file
           ; upar [txt "And running it with:"]
           ; ucodeblock `Shell !ref_cmd
           ; txt "gives:"
           ; ucodeblock `Output dk_output ] ] ) ;
  unit
[@@warning "-unused-var-strict"]
