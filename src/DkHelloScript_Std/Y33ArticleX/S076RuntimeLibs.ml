[@@@warning "-unused-open"]

open Tr1Tezt_C.Tezt
open Tr1Tezt_C.Tezt.Base
open Tr1Htmlit_Std.Htmlit

let register_before_tests () =
  Test.register ~__FILE__ ~title:"runtime libraries" ~tags:[]
  @@ fun () ->
  let open Doc in
  let open El in
  append
    (usection
       [ ucontainer
           [ unsafe_raw
               {section|
        <div class="card block">
            <div class="card-content">
                <p class="title">
                    DkCoder Runtime Libraries
                </p>
            </div>
        </div>

### shexp.process

This is a library for creating possibly parallelized pipelines that represent
shell scripts.

Docs: [shexp README](https://github.com/janestreet/shexp#readme)

<div class="message is-warning">
    <div class="message-header">Warning!</div>
    <div class="message-body">This library only works on 64-bit machines.</div>
</div>

### ocaml-monadic

This small macro (PPX) library provides the `let%bind`, `if%bind` and
`match%bind` forms. These are similar in design to Jane Street's `ppx_let`
macros but work in 32-bit architectures and will be easier to support
backwards-compatibilty on OCaml 4.

This library also offers the `;%bind`, `let%orzero` and `[%guard]` forms.

Docs:
[ocaml-monadic README](https://github.com/zepalmer/ocaml-monadic?tab=readme-ov-file#ocaml-monadic)

|section}
           ] ] ) ;
  unit
  [@@warning "-unused-var-strict"]
