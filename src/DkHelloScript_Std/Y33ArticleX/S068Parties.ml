[@@@warning "-unused-open"]

open Tr1Tezt_C.Tezt
open Tr1Tezt_C.Tezt.Base
open Tr1Htmlit_Std.Htmlit

let register_before_tests () =
  Test.register ~__FILE__ ~title:"parties who own scripts" ~tags:[]
  @@ fun () ->
  let open Doc in
  let open El in
  append
    (usection
       [ ucontainer
           [ unsafe_raw
               {section|
        <div class="card block">
            <div class="card-content">
                <p class="title">
                    Parties Who Own Scripts
                </p>
            </div>
        </div>
        <p class="block">
There are three parties of people who own DkCoder scripts:</p>
<ol class="block">
<li><strong>You</strong> (the first party)</li>
<li><strong>Us</strong> (the second party)</li>
<li><strong>Them</strong> (the third parties)</li>
</ol>

        <p class="block">Here is a handy reference table that summarizes the distinctions. You won't understand the table right now and that is OK. We'll cover "Us" and "Them" in later sections in a future edition of DkCoder, but it is nice to have all the information in one place. All you need to remember is if you have a question about parties, this is the table to check.</p>
<table class="table">
            <caption class="subtitle">Party Reference Table</caption>
            <thead>
                <tr>
                    <th>Party</th>
                    <th>Code Generators</th>
                    <th>Terms</th>
                    <th>Compiled Initially</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Party</th>
                    <th>Code Generators</th>
                    <th>Terms</th>
                    <th>Compiled Initially</th>
                </tr>
            </tfoot>
            <tbody>
                <tr>
                    <th>You</th>
                    <td><code>DuneIde</code> (default), <code>Dune</code></td>
                    <td>Checked</td>
                    <td>Yes</td>
                </tr>
                <tr>
                    <th>Us</th>
                    <td><code>Dune</code></td>
                    <td></td>
                    <td>No</td>
                </tr>
                <tr>
                    <th>Them</th>
                    <td><code>DuneIde</code> (default), <code>Dune</code></td>
                    <td>Checked</td>
                    <td>No</td>
                </tr>
            </tbody>
        </table>
        <p class="block">In this section we'll talk about where your <strong>You</strong> scripts go.</p>
        <p class="block">
            Your scripts must be placed in specific paths.
            Here is an example which you've seen in this walkthrough,
            and the general pattern you must follow:
        </p>
|section}
           ; ucodeblock `Text
               {|
.
└── src/
    ├── DkHelloScript_Std/
    │   └── Example001.ml
    └── <Libraryowner><Project>_<Unit>/
        │── <Modulename1>.ml
        └── <Modulename2>/
            │── <Modulename3>.ml
            │── <Modulename3>.mli
            └── <Modulename4>/
                └── <Modulename5>.ml
|}
           ; unsafe_raw
               {section|

In the example above the `DkHelloScript_Std` is a **library**. A library is an
organization of files and directories. These directories can have
subdirectories, and those subdirectories can have their own subdirectories
(etc.).

Any directory under `src/` that is named in the format:

&lt;Libraryowner&gt;&lt;Project&gt;_&lt;Unit&gt;

is also a library.

<table class="table">
            <caption class="subtitle">Breakdown of the <code>DkHelloScript_Std</code> library name</caption>
            <thead>
                <tr>
                    <th>Component</th>
                    <th>Part</th>
                    <th>Why</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Component</th>
                    <th>Part</th>
                    <th>Why</th>
                </tr>
            </tfoot>
            <tbody>
                <tr>
                    <th><code>Dk</code></th>
                    <td>Library owner</td>
                    <td>A short identifier for which party owns the source code</td>
                </tr>
                <tr>
                    <th><code>HelloScript</code></th>
                    <td>Project</td>
                    <td>Conventionally this is a name related to your source code repository, but it can be any organizational name you want. You might, for example, want to name it your product name.</td>
                </tr>
                <tr>
                    <th><code>Std</code></th>
                    <td>Unit</td>
                    <td>Conventionally if you want to create a single library in a project, you use the name <code>Std</code>. Once you get too much code in your project, you can use the Unit to separate your code into more manageable, smaller pieces.</td>
                </tr>
            </tbody>
</table>
|section}
           ; upar
               [ txt "We'll define the exact format of "
               ; ucode "<Libraryowner>"
               ; txt ", "
               ; ucode "<Project>"
               ; txt ", and "
               ; ucode "<Unit>"
               ; txt " below." ]
           ; unsafe_raw
               {section|

You can place your script into the library directory, or into a subdirectory (or
subdirectory of a subdirectory, etc.) named according to the `<Modulename>/`
format defined below, as long as your script is named according to the
`<Modulename>.ml` format. There is also an optional `<Modulename>.mli` script
interface which is not covered in this article.

|section}
           ; unsafe_raw
               {section|
<table class="table">
            <caption class="subtitle">Rules for the parts of a library name</caption>
            <thead>
                <tr>
                    <th>Part</th>
                    <th>Examples</th>
                    <th>Rules</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Part</th>
                    <th>Examples</th>
                    <th>Rules</th>
                </tr>
            </tfoot>
            <tbody>
                <tr>
                    <th>Library owner</th>
                    <td><code>Dk</code>, <code>Acme</code>, <code>Blue123</code></td>
                    <td>
The Library owner must start with an ASCII capital letter and have a
lowercase ASCII second letter; the remaining characters (if any) must be
lowercase ASCII letters and/or ASCII digits.</td>
                </tr>
                <tr>
                    <th>Project</th>
                    <td><code>X</code>, <code>Ab</code>, <code>Widgets</code>, <code>WidgetsPlus</code></td>
                    <td>
The Project must start with an ASCII capital letter. The remaining
characters (if any) must be ASCII letters (any case) and/or ASCII digits.
Conventionally you use something similar to your source code repository name as the Project.</td>
                </tr>
                <tr>
                    <th>Unit</th>
                    <td><code>Std</code>, <code>V1</code>, <code>X</code></td>
                    <td>
The library unit must start with an ASCII capital letter. The remaining
characters (if any) must be ASCII letters (any case) and/or ASCII digits
and/or underscores (`_`).
Conventially the main unit for your project is named `Std`.
Once you get too much code in your project, you can use the Unit to separate your code into more manageable, smaller pieces.</td>
                </tr>
            </tbody>
        </table>

<div class="message is-info">
<div class="message-header">Carefully choose the library owner</div>
<div class="message-body">
The Library owner (ex. <code>Acme</code> above) should uniquely identify yourself or
your organization. Pick one and use it consistently. If you decide to publish libraries to the DkRegistry (described later) you will avoid conflicts with other libraries.</div>
</div>

|section}
           ; unsafe_raw
               {section|
<table class="table">
            <caption class="subtitle">Rules for a module name</caption>
            <thead>
                <tr>
                    <th>Examples</th>
                    <th>Counter Examples</th>
                    <th>Rule</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th><code>Acme</code>, <code>Acme_Two</code>, <code>X</code></th>
                    <td><code>DkHelloScript_Std</code></td>
                    <td>

The module name MUST NOT be a valid `<Libraryowner><Project>_<Unit>` name.</td>

</tr> <tr> <th><code>Acme</code>, <code>Acme*Two</code>, <code>X</code></th>
<td><code>12345</code>, <code>someThing</code></td> <td> The module name must
start with an ASCII capital letter. The remaining characters (if any) must be
ASCII letters (any case) and/or ASCII digits and/or underscores
(<code>*</code>).</td> </tr> </tbody> </table>
|section}
           ] ] ) ;
  unit
[@@warning "-unused-var-strict"]
