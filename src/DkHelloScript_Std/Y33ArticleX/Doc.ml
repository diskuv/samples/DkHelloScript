open Tr1Htmlit_Std.Htmlit
open Tr1Stdlib_V414Base
open Tr1Stdlib_V414CRuntime
open Tr1Stdlib_V414Io
open Tr1Clap_Std
open Tr1Tezt_C.Tezt
open Tr1Tezt_C.Tezt.Base

let current_doc_format = ref `Html

let q = Queue.create ()

let append elt = Queue.add elt q

(** {1 Switching between pure Markdown and raw HTML modes} *)

let mode = Stack.create ()

(** When switching from within an HTML block element (ex. ["<div>"])
    to Markdown, we need to leave a blank line before and after
    the Markdown (and don't use any left padding on the Markdown).

    Ex.

    {v
    <div class="blah blee">

    Here is some `Markdown`! Visit [apple](apple.com).

    </div>
    v}
    *)
let uprefermarkdownblock f =
  match (!current_doc_format, Stack.top_opt mode) with
  | `Html, _ ->
      f ()
  | `Markdown, None | `Markdown, Some `MarkdownBlock ->
      f ()
  | `Markdown, Some `RawHtmlBlock ->
      Stack.push `MarkdownBlock mode ;
      let y = f () in
      ignore (Stack.pop mode) ;
      El.(splice [unsafe_raw "\n\n"; y; unsafe_raw "\n\n"])

let uhtmlblock f =
  match !current_doc_format with
  | `Html ->
      f ()
  | `Markdown ->
      Stack.push `RawHtmlBlock mode ;
      let y = f () in
      ignore (Stack.pop mode) ;
      y

(** {1 Elements affected by the documentation format}  *)

let project_path file = Printf.sprintf "src/%s" file

let project_url project_path =
  Printf.sprintf
    "https://gitlab.com/diskuv/samples/dkcoder/DkHelloScript/-/blob/main/%s"
    project_path

let usection l = El.(section ~at:[At.class' "section"]) l

let ucontainer l = El.(div ~at:[At.class' "container"]) l

let ucard ~title:title_ l =
  El.(
    div
      ~at:[At.class' "card"; At.class' "block"]
      [ div
          ~at:[At.class' "card-content"]
          [p ~at:[At.class' "title"] [txt title_]; splice l] ] )

let upar items =
  match (!current_doc_format, Stack.top_opt mode) with
  | `Html, _ | `Markdown, Some `RawHtmlBlock ->
      El.(p ~at:[At.class' "block"]) items
  | `Markdown, None | `Markdown, Some `MarkdownBlock ->
      (* Blank line before to ensure a Markdown paragraph. *)
      El.(splice (unsafe_raw "\n\n" :: items))

let uinfo ?header body_ =
  let header_ =
    match header with
    | None ->
        El.splice []
    | Some hdr ->
        El.(div ~at:[At.class' "message-header"] hdr)
  in
  El.(
    div
      ~at:[At.class' "message"; At.class' "is-info"]
      [header_; div ~at:[At.class' "message-body"] body_] )

let uol (l : El.html list) =
  match (!current_doc_format, Stack.top_opt mode) with
  | `Html, _ | `Markdown, Some `RawHtmlBlock ->
      El.(ol ~at:[At.class' "block"]) (List.map (fun x -> El.li [x]) l)
  | `Markdown, None | `Markdown, Some `MarkdownBlock ->
      let s' =
        String.concat "\n"
          (List.map
             (fun x ->
               Fmt.str "@[<3>1. %a@]" Fmt.lines (El.to_string ~doctype:false x)
               )
             l )
      in
      (* MD032/blanks-around-lists: Lists should be surrounded by blank lines markdownlint MD032 *)
      El.(unsafe_raw (Printf.sprintf "\n\n%s\n\n" s'))

let uul (l : El.html list) =
  match (!current_doc_format, Stack.top_opt mode) with
  | `Html, _ | `Markdown, Some `RawHtmlBlock ->
      El.(ul ~at:[At.class' "block"]) (List.map (fun x -> El.li [x]) l)
  | `Markdown, None | `Markdown, Some `MarkdownBlock ->
      let s' =
        String.concat "\n"
          (List.map
             (fun x ->
               Fmt.str "@[<2>- %a@]" Fmt.lines (El.to_string ~doctype:false x)
               )
             l )
      in
      (* MD032/blanks-around-lists: Lists should be surrounded by blank lines markdownlint MD032 *)
      El.(unsafe_raw (Printf.sprintf "\n\n%s\n\n" s'))

let ucode content =
  match (!current_doc_format, Stack.top_opt mode) with
  | `Html, _ | `Markdown, Some `RawHtmlBlock ->
      El.(code [txt content])
  | `Markdown, None | `Markdown, Some `MarkdownBlock ->
      El.(unsafe_raw (Printf.sprintf "`%s`" content))

let ulink ~url content =
  match (!current_doc_format, Stack.top_opt mode) with
  | `Html, _ | `Markdown, Some `RawHtmlBlock ->
      El.(a ~at:[At.href url] [txt content])
  | `Markdown, None | `Markdown, Some `MarkdownBlock ->
      El.(unsafe_raw (Printf.sprintf "[%s](%s)" content url))

let ucodeblock =
  let trim code' =
    let open Re in
    let nl = seq [opt (set "\r"); set "\n"] in
    let space_not_nl = diff space (set "\r\n") in
    let first_blank_line = compile (seq [bos; rep space_not_nl; nl]) in
    let last_blank_line = compile (seq [nl; rep space_not_nl; eos]) in
    code'
    |> replace_string first_blank_line ~by:""
    |> replace_string last_blank_line ~by:""
  in
  fun lang code' ->
    let lang =
      match lang with
      | `Json ->
          "json"
      | `Shell ->
          "sh"
      | `OCaml ->
          "ocaml"
      | `Output ->
          "output"
      | `Text ->
          "text"
    in
    let code' = trim code' in
    match (!current_doc_format, Stack.top_opt mode) with
    | `Html, _ | `Markdown, Some `RawHtmlBlock ->
        El.(pre [code ~at:[At.class' ("language-" ^ lang)] [txt code']])
    | `Markdown, None | `Markdown, Some `MarkdownBlock ->
        (* Recommendations say that fenced code blocks should have blank line
           before and after.
           Confer:
           https://github.com/markdownlint/markdownlint/blob/main/docs/RULES.md#md031---fenced-code-blocks-should-be-surrounded-by-blank-lines
           https://docs.github.com/en/get-started/writing-on-github/working-with-advanced-formatting/creating-and-highlighting-code-blocks
        *)
        El.(unsafe_raw (Printf.sprintf {|

```%s
%s
```

|} lang code' ))

let ucodefile file =
  let content =
    In_channel.with_open_text (project_path file) (fun ic ->
        In_channel.input_all ic )
  in
  ucodeblock `OCaml content

let ucodeaction ~ide ~browser file =
  let project_path = project_path file in
  let browser_content =
    match browser with
    | None ->
        El.void
    | Some browser ->
        El.(
          splice
            [ txt " or "
            ; txt browser
            ; txt " "
            ; ulink ~url:(project_url project_path) project_path
            ; txt " in your browser" ] )
  in
  uprefermarkdownblock (fun () ->
      El.(
        splice
          [ unsafe_raw "\n"
          ; ide
          ; txt " "
          ; ucode project_path
          ; txt " in your IDE"
          ; browser_content
          ; txt "."
          ; unsafe_raw "\n\n" ] ) )

let print style () =
  let buf = Buffer.create 1024 in
  let fmt = Format.formatter_of_buffer buf in
  let content = El.splice (Queue.to_seq q |> List.of_seq) in
  let doctype = match style with `Html -> true | `Markdown -> false in
  let html =
    match style with
    | `Html ->
        let body_content =
          (* Place <script src="prism.js"> at bottom of body *)
          let prismjs_src =
            [At.src "https://cdn.jsdelivr.net/npm/prismjs@1.29.0/prism.min.js"]
          in
          let autoloader_src =
            [ At.src
                "https://cdn.jsdelivr.net/npm/prismjs@1.29.0/plugins/autoloader/prism-autoloader.min.js"
            ]
          in
          El.(
            splice
              [content; script ~at:prismjs_src []; script ~at:autoloader_src []] )
        in
        (* Generate all the <html><head>...</head><body>...</body></html> structure *)
        let body = El.body [body_content] in
        El.page ~lang:"en" ~generator:"DkCoder"
          ~styles:
            [ "https://cdn.jsdelivr.net/npm/prismjs@1.29.0/themes/prism-okaidia.min.css"
            ; "https://cdn.jsdelivr.net/npm/bulma@1.0.0/css/bulma.min.css" ]
          ~title:__MODULE_ID__ body
    | `Markdown ->
        content
  in
  (* Print header *)
  begin
    match style with
    | `Html ->
        ()
    | `Markdown ->
        Format.pp_print_string fmt
          {|---
title: Intro to DkCoder Scripting
type: article
description: Scripting is a small, free and important piece of DkSDK.
slug: dksdk/coder/2024-intro-scripting
date: 2024-02-12
publish_to: Prod
---
|}
  end ;
  (* Print body *)
  Format.fprintf fmt "%s" (El.to_string ~doctype html) ;
  (* Print footer *)
  begin
    match style with `Html -> () | `Markdown -> ()
  end ;
  Format.pp_print_flush fmt () ;
  String.of_bytes (Buffer.to_bytes buf)

let register_before_tests () =
  let section =
    Clap.section "DOCUMENTATION"
      ~description:
        "Documentation is collected as a side-effect of exercising the source \
         code (that is, running tests) in this project. Use one of the options \
         in this section to specify where you want the documentation to be \
         produced -AND- make sure the 'write documentation' test runs."
  in
  (* Add [--doc-file] command line option *)
  let opt_file =
    Clap.optional_string ~section ~description:"Write documentation to file."
      ~long:"doc-file" ~placeholder:"FILE" ()
  in
  (* Add [--doc] command line option *)
  let flag_stdout =
    Clap.flag ~section ~description:"Print documentation to the console."
      ~set_long:"doc" ~unset_long:"no-doc" false
  in
  (* Add [--doc-format] command line option *)
  let doc_format_typ =
    Clap.typ ~name:"documentation format" ~dummy:!current_doc_format
      ~parse:(function
        | "html" -> Some `Html | "markdown" -> Some `Markdown | _ -> None )
      ~show:(function `Html -> "html" | `Markdown -> "markdown")
  in
  let doc_format =
    Clap.default doc_format_typ ~section
      ~description:"Set the documentation format. Either html or markdown"
      ~long:"doc-format" ~placeholder:"FORMAT" !current_doc_format
  in
  (* Export the --doc-format to the rest of [Doc] *)
  current_doc_format := doc_format ;
  (* Make a test that writes the documentation *)
  Test.register ~__FILE__ ~title:"write documentation" ~tags:["documentation"]
  @@ fun () ->
  begin
    match opt_file with
    | Some file ->
        Out_channel.with_open_text file (fun oc ->
            Out_channel.output_string oc (print doc_format ()) )
    | None ->
        ()
  end ;
  if flag_stdout then StdIo.print_endline (print doc_format ()) ;
  unit
