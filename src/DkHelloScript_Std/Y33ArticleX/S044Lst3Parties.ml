[@@@warning "-unused-open"]

open Tr1Tezt_C.Tezt
open Tr1Tezt_C.Tezt.Base
open Tr1Htmlit_Std.Htmlit

let register_before_tests () =
  Test.register ~__FILE__ ~title:"listing third parties" ~tags:[]
  @@ fun () ->
  let open Doc in
  let open El in
  append
    (usection
       [ ucontainer
           [ unsafe_raw
               {section|
        <div class="card block">
            <div class="card-content">
                <p class="title">
                    Listing third parties
                </p>
            </div>
        </div>
        <div class="block">
            There are three parties of people whose scripts you can use:
            <ol>
            <li>You - the first party</li>
            <li>Us (the developers of DkCoder) - the second party</li>
            <li>Them (other developers) - the third parties. Third parties can either be:
                <ul>
                    <li><strong>listed</strong> in a <code>dkproject.jsonc</code> file</li>
                    <li><strong>registered</strong> with the <code>DkRegistry</code></li>
                </ul>
            </li>
            </ol>
        </div>
        <div class="block">In this section we'll use <em>listed</em> third parties.</div>
        <div class="message is-info">
            <div class="message-body">
                For <strong>listed</strong> third parties, you say what you want to get, and DkCoder will fetch it for you.</div>
        </div>
        <p class="block">
            Let's write a <code>dkproject.jsonc</code> file inside your project folder that says we want a demo of someone else's source code using the <code>sanette-in-article</code> branch:
        </p>

```json
{
  "dependencies": {
    "SanetteBogueDemo_Std": {
      "urls": [
        "https://github.com/jonahbeckford/bogue-demo.git#sanette-in-article"
      ]
    }
  }
}
```

<p class="block">Now we can run it.</p>

```sh
./dk DkRun_V0_4.Run SanetteBogueDemo_Std.DemoMain
```

        <p class="block">Go ahead and watch the demo video as well:</p>
        <figure class="block image is-16by9">
            <iframe title="bogue demo v1907" class="has-ratio" width="640" height="360"
                src="https://www.youtube.com/embed/isFLxnDooL8?si=t47f_6h-hOxPfoxu" allowfullscreen></iframe>
        </figure>
    </div>

</section>
|section}
           ] ] ) ;
  unit
  [@@warning "-unused-var-strict"]
