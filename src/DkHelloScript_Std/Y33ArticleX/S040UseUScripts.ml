[@@@warning "-unused-open"]

open Tr1Tezt_C.Tezt
open Tr1Tezt_C.Tezt.Base
open Tr1Htmlit_Std.Htmlit

let register_before_tests () =
  Test.register ~__FILE__ ~title:"reusing your own scripts" ~tags:[]
  @@ fun () ->
  let open Doc in
  let open El in
  append
    (usection
       [ ucontainer
           [ unsafe_raw
               {section|
        <div class="card block">
            <div class="card-content">
                <p class="title">
                    Re-using your own scripts
                </p>
            </div>
        </div>
        <p class="block">Use a module signature.</p>
        <p class="block">Add <code>dkproject.jsonc</code> with <code>_local</code> inheritance.</p>
|section}
           ] ] ) ;
  unit
  [@@warning "-unused-var-strict"]
