[@@@warning "-unused-open"]

open Tr1Tezt_C.Tezt
open Tr1Tezt_C.Tezt.Base
open Tr1Htmlit_Std.Htmlit

let register_before_tests () =
  Test.register ~__FILE__ ~title:"release notes - early tech limits" ~tags:[]
  @@ fun () ->
  let open Doc in
  let open El in
  append
    (usection
       [ ucontainer
           [ unsafe_raw
               {section|
        <div class="card block">
            <div class="card-content">
                <p class="title">
                    Release Notes - Early Technical Limitations
                </p>
            </div> |section}
           ; uol
               [ splice
                   [ txt "On first install for Windows running the "
                   ; ucode
                       ( "./dk " ^ Tr1Version.run_module
                       ^ " -- DkHelloScript_Std.Y33Article --serve" )
                   ; txt " example can give:"
                   ; ucodeblock `Output
                       {|
[00:00:22.564] [SUCCESS] (3/18) reproducibility or quick typing
[00:00:22.564] Starting test: focus on what you run
[00:00:22.566] [.\dk.cmd#3] '.\dk.cmd' DkRun_V2_1.Run '--generator=dune' -- DkHelloScript_Std.AndHelloAgain
[ERROR][2024-04-29T00:00:43Z] /Run/
       Failed to run
         C:\Users\WDAGUtilityAccount\DkHelloScript\src\DkHelloScript_Std\Y33Article.ml
         (DkHelloScript_Std.Y33Article). Code fa10b83d.

       Problem: The DkHelloScript_Std.Y33Article script exited with
         STATUS_ACCESS_VIOLATION (0xC0000005) - The instruction at 0x%08lx
       referenced memory at 0x%08lx. The memory could not be %s.
       Solution: Scroll up to see why.
|}
                   ; txt "If you rerun it, it succeeds." ]
               ; splice
                   [ unsafe_raw
                       "The <code>./dk</code> script delegates to the build \
                        tool <code>CMake</code> first, "
                   ; unsafe_raw "and then to <code>OCaml</code>. "
                   ; txt
                       "That design reflects the history that DkCoder was \
                        designed first for C packages. "
                   ; txt "The current design means:"
                   ; unsafe_raw
                       {section|
<ol>
    <li>
        <code>./dk</code> has the performance overhead of spawning CMake on startup.
    </li>
    <li>
        Ctrl-C does not cleanly kill all of the subprocesses on Windows. You may need to run
        <code>taskkill /f /im ocamlrunx.exe</code> to kill these hung processes.
    </li>
    <li>
        <code>./dk</code> has command line arguments interpreted by CMake before being interpreted by your script. In particular, nested double-quotes like <code>./dk ... "here is a nested `" double quote"</code> in PowerShell won't work.
    </li>
</ol>
|section}
                   ]
               ; txt
                   "The GUI code was very recently ported to Windows. It has \
                    not had memory auditing so segfaults may occur. And \
                    playing sounds is known to hang. Do not use for GUIs and \
                    music for production code until we message that this has \
                    been fixed." ] ] ] ) ;
  unit
[@@warning "-unused-var-strict"]
