[@@@warning "-unused-open"]

open Tr1Tezt_C.Tezt
open Tr1Tezt_C.Tezt.Base
open Tr1Htmlit_Std.Htmlit

let register_before_tests () =
  Test.register ~__FILE__ ~title:"runtime system" ~tags:[]
  @@ fun () ->
  let open Doc in
  let open El in
  append
    (usection
       [ ucontainer
           [ unsafe_raw
               {section|
        <div class="card block">
            <div class="card-content">
                <p class="title">
                    Runtime system
                </p>
            </div>
        </div>

Your scripts will have access to the following libraries:

- curl
  - All OSes:
    - Brotli and zstd compression
    - Asynchronous DNS resolution
    - HTTP/2
    - Websockets
  - Windows: Schannel TLS backend (Microsoft CryptoAPI) and WinIDN domain name resolution
  - macOS: Secure transport backend (Keychain) and IDN2
  - Linux: Openssl is required from the operating system. DkCoder does not provide it.
- SDL2
|section}
           ] ] ) ;
  unit
  [@@warning "-unused-var-strict"]
