open Tr1Tezt_C.Tezt
open Tr1Tezt_C.Tezt.Base
open Tr1Htmlit_Std.Htmlit

let register_before_tests ~andhello_module () =
  Test.register ~__FILE__ ~title:"reproducibility or quick typing" ~tags:[]
  @@ fun () ->
  let ref_cmd, hooks = RunDk.make_capture () in
  let* dk_output = RunDk.run_dk ~hooks [andhello_module] in
  let open Doc in
  let open El in
  append
    (usection
       [ ucontainer
           [ ucard ~title:"Reproducibility or quick typing? Pick one" []
           ; upar
               [ txt
                   "We specified a version number and a double dash separator ("
               ; ucode (Tr1Version.run_module ^ " --")
               ; txt ") in our last example:" ]
           ; ucodeblock `Shell !ref_cmd
           ; upar [txt "You can relax your fingers by instead typing:"]
           ; ucodeblock `Shell ("./dk " ^ andhello_module)
           ; upar
               [ txt "In your everyday scripting you won't want to type "
               ; ucode (Tr1Version.run_module ^ " --")
               ; txt ".\n"
               ; strong [txt "Leave it out"]
               ; txt ".\n" ]
           ; upar
               [ unsafe_raw
                   {section|
However, when you are publishing documentation (like this article) you should
<em>always</em> include the version number. You'll find it easy for your users
to copy-and-paste and more importantly, the behavior of your scripts won't
change in some future version of
|section}
               ; ucode "./dk"
               ; txt "." ] ] ] ) ;
  unit
[@@warning "-unused-var-strict"]
