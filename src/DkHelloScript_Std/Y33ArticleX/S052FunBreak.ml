[@@@warning "-unused-open"]

open Tr1Tezt_C.Tezt
open Tr1Tezt_C.Tezt.Base
open Tr1Htmlit_Std.Htmlit

let register_before_tests () =
  Test.register ~__FILE__ ~title:"fun break" ~tags:[]
  @@ fun () ->
  let open Doc in
  let open El in
  append
    (usection
       [ ucontainer
           [ unsafe_raw
               {section|
        <div class="card block">
            <div class="card-content">
                <p class="title">
                    Fun break!
                </p>
            </div>
        </div>
        <p class="block">
            For fun, let's run a complete game: Snóke. This excellent demo is from the same author
            <a href="https://github.com/sanette">https://github.com/sanette</a> and has the same
            <a href="https://raw.githubusercontent.com/sanette/bogue-demo/main/LICENSE">GPL 3.0</a>
            license.
            In your <code>dkproject.jsonc</code> file modify it so it looks like:
        </p>

```json
{
  "dependencies": {
    "SanetteBogueDemo_Std": {
      "urls": [
        "https://github.com/jonahbeckford/bogue-demo.git#sanette-in-article"
      ]
    },
    "SanetteBogueSnoke_Std": {
      "urls": ["https://github.com/jonahbeckford/snoke.git#sanette-in-article"]
    }
  }
}
```

<p class="block">To run it:</p>

```sh
./dk DkRun_V0_4.Run SanetteBogueSnoke_Std.SnokeMain
```

<p class="block">You can watch the video as well:</p>
        <figure class="block image is-16by9">
            <iframe title="bogue demo v1907" class="has-ratio" width="640" height="360"
                src="https://www.youtube.com/embed/h1MC9-xDKFA?si=OxbTnYb5rJb76oA-" allowfullscreen></iframe>
        </figure>
|section}
           ] ] ) ;
  unit
  [@@warning "-unused-var-strict"]
