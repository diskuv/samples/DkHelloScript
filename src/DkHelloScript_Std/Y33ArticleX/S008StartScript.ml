open Tr1Tezt_C.Tezt
open Tr1Tezt_C.Tezt.Base
open Tr1Htmlit_Std.Htmlit

let register_before_tests ~andhello_module ~andhello_file () =
  Test.register ~__FILE__ ~title:"start scripting" ~tags:[]
  @@ fun () ->
  let ref_cmd, hooks = RunDk.make_capture () in
  let* dk_output = RunDk.run_dk ~hooks [andhello_module] in
  let open Doc in
  let open El in
  append
    (usection
       [ ucontainer
           [ unsafe_raw
               {section|
        <div class="card block">
            <div class="card-content">
                <p class="title">
                    You, your co-developers and your users can start scripting in a couple clicks.
                </p>
            </div>
        </div>
        <p class="block">
            <strong>FIRST</strong>, if you don't have these installed then install <em>Git</em> from <a href="https://git-scm.com/downloads">https://git-scm.com/downloads</a> and <em>Visual Studio Code</em> from
            <a href="https://code.visualstudio.com/download">https://code.visualstudio.com/download</a>.
        </p>
        <p class="block">Then click this link:
            <a href="vscode://vscode.git/clone?url=https%3A%2F%2Fgitlab.com%2Fdiskuv%2Fsamples%2Fdkcoder%2FDkHelloScript.git">Clone
                and Open DkHelloScript in Visual Studio Code</a>.
        </p>
        <div class="message is-primary">
            <div class="message-body">
                Click to <em><strong>Trust the Authors</strong></em> and
                <em><strong>Install the Recommended Extensions!</strong></em>
            </div>
        </div>
|section}
           ; div
               ~at:[At.class' "message is-dark ml-3 is-small"]
               [ div
                   ~at:[At.class' "message-header"]
                   [txt "Want to Use The Command Line Instead?"]
               ; ucodeblock `Shell
                   {|
git clone https://gitlab.com/diskuv/samples/dkcoder/DkHelloScript.git
code DkHelloScript
|}
               ]
           ; upar
               [ ucodeaction
                   ~ide:(splice [strong [txt "SECOND"]; txt ", open "])
                   ~browser:(Some "open") andhello_file
               ; txt "You should see:" ]
           ; ucodefile andhello_file
           ; unsafe_raw
               {section|

<div class="message is-info">
            <div class="message-body">Wait a minute or two for the one-time background installation, and then you'll notice the text colors have changed.
            Hover over the <code>print_endline</code> to see its API.
            <em>After DkCoder gets out of Alpha, there will be a progress bar to provide visual feedback during the background install.</em></div>
        </div>
        <p class="block">
            <strong>THIRD</strong>,
            open a <code>Terminal > New Terminal</code> and run:
        </p>
|section}
           ; ucodeblock `Shell !ref_cmd
           ; ucodeblock `Output dk_output
           ; unsafe_raw
               {section|
<p class="block">
            <span class="icon-text">
                <span class="icon has-text-success">
  <img
    alt="Checkmark"
    class="icon-ionic-md-checkmark-circle"
    src="/img/icon-ionic-md-checkmark-circle-10@1x.png"
  />
                </span>
                <span>
                    <strong>DONE!</strong>
                    Ok, one-liners are not very interesting.
                </span>
            </span>
        </p>
        <p class="block">
            <em>But</em> ... that should have only taken a few minutes.
        </p>
        <p class="block">
            <em>And</em> ... the same command works on Windows, macOS and GNU/Linux.
        </p>
    </div>
    <div class="notification is-warning">
            <button class="delete"></button>
            <div class="block">
                You will be prompted to accept the DkCoder licenses.
                The game demo has a separate license: <a
                    href="https://raw.githubusercontent.com/sanette/bogue-demo/main/LICENSE">GPL 3.0</a>.
                If you want to adapt the game demo for commercial purposes you must contact the demo author
                at <a href="https://github.com/sanette">https://github.com/sanette</a>.
            </div>
            <div class="block">
                We'll go over licensing in general later in this walkthrough.
            </div>
|section}
           ] ] ) ;
  unit
[@@warning "-unused-var-strict"]
