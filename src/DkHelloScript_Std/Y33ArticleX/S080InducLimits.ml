[@@@warning "-unused-open"]

open Tr1Tezt_C.Tezt
open Tr1Tezt_C.Tezt.Base
open Tr1Htmlit_Std.Htmlit

let register_before_tests () =
  Test.register ~__FILE__ ~title:"limitations imposed by cross-platform support"
    ~tags:[]
  @@ fun () ->
  let open Doc in
  let open El in
  append
    (usection
       [ ucontainer
           [ unsafe_raw
               {section|
        <div class="card block">
            <div class="card-content">
                <p class="title">
                    Limitations imposed by cross-platform support.
                </p>
            </div>
        </div>
        <p class="block">
        DkSDK is a family of tools that supports cross-platform development, even on
        embedded devices. This cross-platform support places constraints on how you
        structure your DkCoder projects.
        </p>
        <ul>
<li>The <strong>FAT32</strong> filesystem is found on older Windows disks, CD drives and on
  embedded devices. FAT32 only supports filenames up to 255 characters. DkCoder
  limits the paths
  <code>&lt;Libraryowner>&lt;Libraryproject>_&lt;Libraryunit>/&lt;Modulename>/.../&lt;Modulename>.mli</code>
  to 240 characters. Even when the FAT32 filesystem entirely disappears from use
  this 240 character limit may remain since the limit encourages modular,
  single-focused libraries.</li>
<li>The <strong>FAT16</strong> filesystem is the only filesystem in moderate use today that is not
  supported by DkCoder. FAT16 might still be used on your older USB thumbdrives
  and some very old embedded devices. Since FAT16 directory names are limited to
  8 uppercase characters, and the minimum
  <code>&lt;Libraryowner>&lt;Libraryproject>_&lt;Libraryunit></code> name is 7 characters, it is not
  reasonable to support FAT16.</li>
        </ul>
|section}
           ] ] ) ;
  unit
  [@@warning "-unused-var-strict"]
