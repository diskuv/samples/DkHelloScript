open Tr1Tezt_C.Tezt
open Tr1Tezt_C.Tezt.Base
open Tr1Htmlit_Std.Htmlit
module In_channel = Tr1Stdlib_V414CRuntime.In_channel
module Filename = Tr1Stdlib_V414CRuntime.Filename
module Printf = Tr1Stdlib_V414CRuntime.Printf

let register_before_tests () =
  Test.register ~__FILE__ ~title:"snoke game" ~tags:["graphics"]
  @@ fun () ->
  let open Doc in
  let open El in
  let png_base64 =
    let share = Tr1Assets.LocalDir.v () in
    let png_binary =
      In_channel.with_open_bin (Filename.concat share "S038SnokeGame_Start.png")
        (fun ic -> In_channel.input_all ic )
    in
    Base64.encode_string ~pad:false png_binary
  in
  append
    (usection
       [ ucontainer
           [ ucard ~title:"The Snoke game" []
           ; upar
               [ txt
                   "So far you have seen a very simple use of the Bogue \
                    graphics library." ]
           ; upar
               [ txt
                   "The author of Bogue has created a demonstration game which \
                    was “ported” to DkCoder." ]
           ; upar
               [ txt
                   "The port did not change a single line of the original \
                    code. The directory structure was re-arranged (recall that \
                    there is a Java-like package mechanism underneath DkCoder) \
                    and an extra "
               ; ucode ".ml"
               ; txt " file was added." ]
           ; upar
               [ txt
                   "Run it outside your project (perhaps your home directory) \
                    with:" ]
           ; ucodeblock `Shell
               {|
git clone --branch V2_1 https://gitlab.com/diskuv/samples/dkcoder/SanetteBogue.git

./SanetteBogue/dk DkRun_V2_1.Run -- SanetteBogue_Snoke.Snoke
|}
           ; upar
               [ txt "You should see a really fun game!"
               ; unsafe_raw
                   (Printf.sprintf
                      "<img alt=\"sanette's Snoke game\" height=\"441px\" \
                       width=\"414px\" src=\"data:image/png;base64,%s\" />"
                      png_base64 ) ]
           ; upar
               [ txt "You can explore its GPL-3.0 licensed source code at "
               ; ulink
                   ~url:
                     "https://gitlab.com/diskuv/samples/dkcoder/SanetteBogue.git"
                   "https://gitlab.com/diskuv/samples/dkcoder/SanetteBogue.git"
               ] ] ] ) ;
  unit
[@@warning "-unused-var-strict"]
