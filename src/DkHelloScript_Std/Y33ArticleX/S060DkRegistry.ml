[@@@warning "-unused-open"]

open Tr1Tezt_C.Tezt
open Tr1Tezt_C.Tezt.Base
open Tr1Htmlit_Std.Htmlit

let register_before_tests () =
  Test.register ~__FILE__ ~title:"dkregistry" ~tags:[]
  @@ fun () ->
  let open Doc in
  let open El in
  append
    (usection
       [ ucontainer
           [ unsafe_raw
               {section|
        <div class="card block">
            <div class="card-content">
                <p class="title">
                    DkRegistry
                </p>
            </div>
        </div>
        <p class="block">Publishing to the DkRegistry is one of the benefits of being a DkSDK subscriber.
            You can sign up to be a subscriber at <a
                href="https://diskuv.com/pricing">https://diskuv.com/pricing</a>.
            After that, you can send a support request with the location of your script, and someone from the
            DkSDK team (that would be me or an intern today!) will set up a continuous integration pipeline to
            pre-compile your script, and a know-your-publisher video session for security.</p>
        <div class="message is-info">
            <div class="message-body">You do <em>not</em> need to be a DkSDK subscriber to use the centralized
                DkRegistry scripts. The only restriction is who can publish to the DkRegistry.</div>
        </div>
        <p>We have a few goals with the DkRegistry:</p>
        <ol class="block">
            <li>
                ensure that your centralized scripts are compatible with all other scripts in the DkRegistry.
                No bitrot! <em>Accomplishing that is much harder than it sounds, and is a primary reason
                    why only DkSDK subscribers get the publishing benefit.</em>
            </li>
            <li>
                maintain a level of security with the centralized scripts. <em>Today that boils down to "know your
                    publisher". The level of security will improve over time.</em>
            </li>
            <li>
                <div class="block">
                    give DkCoder users (a student, a professional developer in a big
                    company, a startup founder, a technology consultant, or even a non-tech
                    user) the clear freedom to use your scripts.
                </div>
                <div class="message is-dark ml-5">
                    <div class="message-header">Third Party Licenses</div>
                    <div class="message-body">
                        <div class="block">
                            Since DkRegistry places first priority on the needs of the DkCoder user,
                            some licensing models won't be accepted in DkRegistry.
                        </div>
                        <div class="content block">
                            <ul>
                                <li>
                                    <a href="https://www.gnu.org/licenses/agpl-3.0.en.html">AGPL-3.0</a>
                                    has contested legal implications related to linking to AGPL source code over
                                    the Internet. Although some companies have banned AGPL, it is quite normal
                                    for a startup to use AGPL to protect itself.
                                    So if you are a startup, you can publish in the DkRegistry with a dual license
                                    AGPL and a commercial license. The commercial license must have a publicly
                                    available pricing list with (at minimum) a USD price. You are likely doing
                                    that anyway, and doing so will help the DkCoder user know upfront what
                                    their scripting project will cost.
                                </li>
                                <li>
                                    <a href="https://www.gnu.org/licenses/lgpl-3.0.en.html">LGPL-3.0</a>
                                    is fine <em>with a static linking exception</em>. DkCoder
                                    will have a feature to create a single-binary executable.
                                    A DkCoder user must never be surprised to find late in a project that they weren't
                                    allowed to distribute the single-binary executable to their customers.
                                    You can also publish in the DkRegistry with a dual license.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
        </ol>
        <table class="table">
            <caption class="subtitle">Comparison between Distributed and Centralized Fetching</caption>
            <thead>
                <tr>
                    <th><abbr title="Feature">#</abbr></th>
                    <th>Description</th>
                    <th>Distributed</th>
                    <th>Centralized</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th><abbr title="Feature">#</abbr></th>
                    <th>Description</th>
                    <th>Distributed</th>
                    <th>Centralized</th>
                </tr>
            </tfoot>
            <tbody>
                <tr>
                    <th>1</th>
                    <td>Anyone can use</td>
                    <td>Yes</td>
                    <td>Yes</td>
                </tr>
                <tr>
                    <th>2</th>
                    <td>Who can publish</td>
                    <td>Anybody</td>
                    <td>Only DkSDK subscribers</td>
                </tr>
                <tr>
                    <th>3</th>
                    <td>Users need to change <code>dkproject.jsonc</code></td>
                    <td>Required</td>
                    <td>Not needed</td>
                </tr>
                <tr>
                    <th>4</th>
                    <td>Performance</td>
                    <td>Slow, unless Centralized packages are imported</td>
                    <td>Fast</td>
                </tr>
                <tr>
                    <th>5</th>
                    <td>Access to C and Java</td>
                    <td>No</td>
                    <td>Yes</td>
                </tr>
                <tr>
                    <th>6</th>
                    <td>Access to binary assets</td>
                    <td>No</td>
                    <td>Yes</td>
                </tr>
                <tr>
                    <th>7</th>
                    <td>Licenses allowed</td>
                    <td>Any</td>
                    <td>Subject to restrictions</td>
                </tr>
                <tr>
                    <th>8</th>
                    <td>Prone to bitrot and version incompatibility</td>
                    <td>Yes</td>
                    <td>No</td>
                </tr>
            </tbody>
        </table>
|section}
           ] ] ) ;
  unit
  [@@warning "-unused-var-strict"]
