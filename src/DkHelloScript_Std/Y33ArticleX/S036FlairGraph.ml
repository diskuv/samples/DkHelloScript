open Tr1Tezt_C.Tezt
open Tr1Tezt_C.Tezt.Base
open Tr1Htmlit_Std.Htmlit
module In_channel = Tr1Stdlib_V414CRuntime.In_channel
module Filename = Tr1Stdlib_V414CRuntime.Filename
module Printf = Tr1Stdlib_V414CRuntime.Printf

let register_before_tests ~boguetiny_module ~boguetiny_file () =
  Test.register ~__FILE__ ~title:"adding flair with graphics" ~tags:["graphics"]
  @@ fun () ->
  let ref_cmd, hooks = RunDk.make_capture () in
  let* dk_output =
    (* Stop graphics from popping up during test *)
    RunDk.run_dk
      ~env:(String_map.singleton "SDL_VIDEODRIVER" "dummy")
      ~expect_failure:true ~hooks [boguetiny_module]
  in
  Check.(
    ( dk_output
    =~ rex
         "Error creating SDL Window: OpenGL support is either not configured \
          in SDL or not available in current SDL video driver" )
      ~error_msg:"expected value =~ %R, got %L" ) ;
  let open Doc in
  let open El in
  let png_base64 =
    let share = Tr1Assets.LocalDir.v () in
    let png_binary =
      In_channel.with_open_bin
        (Filename.concat share "S036FlairGraph_Hello_world.png") (fun ic ->
          In_channel.input_all ic )
    in
    Base64.encode_string ~pad:false png_binary
  in
  append
    (usection
       [ ucontainer
           [ ucard ~title:"Adding some flair with graphics" []
           ; upar
               [ ucodeaction ~ide:(txt "Open") ~browser:(Some "open")
                   boguetiny_file
               ; txt "You should see:" ]
           ; ucodefile boguetiny_file
           ; upar [txt "Run it with:"]
           ; ucodeblock `Shell !ref_cmd
           ; upar
               [ txt "to see:"
               ; unsafe_raw
                   (Printf.sprintf
                      "<img alt=\"Bogue's Hello World\" \
                       src=\"data:image/png;base64,%s\" />"
                      png_base64 ) ] ] ] ) ;
  unit
[@@warning "-unused-var-strict"]
