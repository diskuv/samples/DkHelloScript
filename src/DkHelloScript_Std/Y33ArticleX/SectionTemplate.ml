[@@@warning "-unused-open"]

open Tr1Tezt_C.Tezt
open Tr1Tezt_C.Tezt.Base
open Tr1Htmlit_Std.Htmlit

let register_before_tests () =
  Test.register ~__FILE__ ~title:"startscripting" ~tags:[]
  @@ fun () ->
  let open Doc in
  append El.(usection [ucontainer [unsafe_raw {section|
|section}]]) ;
  unit
  [@@warning "-unused-var-strict"]
