open Tr1Htmlit_Std.Htmlit
open Tr1Stdlib_V414CRuntime

let content = El.(a ~at:[At.href "diskuv.com"] [txt "Hi Builder!"])

let page = El.(html [head [title [txt "Example310"]]; body [content]])

let () = Format.printf "%s%!" (El.to_string ~doctype:true page)
