open Tr1Tezt_C.Tezt
open Tr1Htmlit_Std.Htmlit

(** {1 Register Tests incl. Documentation and Server}
    
    If you are familiar with React, you can "push down" functions and values
    so that your deeper content can render itself. We do the same thing
    here, and that is why you see all the [~make_capture ~run_dk] and other
    parameters.
    
    We could have also used a `src/DkHelloScript_Std/open__.ml` file
    to provide values to all the scripts of the [DkHelloScript_Std] library.
    If you are familiar with React, that "global" style of distributing
    values would be called a "context".

    Even simpler would be to create functions inside [Doc] since
    [Y33ArticleX/Doc] is visible to the [Y33ArticleX/Section*] scripts. 
    
    If possible find a module like [Doc] that is visible to all the scripts
    that need it, and place your functions in the module. We don't do it
    in this project except for one function {!ucodeblock} for demonstration
    purposes.

    If that doesn't work for the way your have organized your project code,
    prefer the "push down" method over the "global context". It is simpler
    to test, your functions will be re-usable outside of the current
    library (ex. outside of [DkHelloScript_Std]), and you won't pull in
    unnecessary dependencies for scripts that don't need it.
    
    You can avoid "unused parameters" errors with the "push down" method
    by attaching the following extension on your "push down" functions:
    
    [[
      let register_before_tests (* ... *) =
         (* ... *)
         unit
         [@@warning "-unused-var-strict"]
    ]] *)

let title_section () =
  Y33ArticleX.Doc.(
    append
      (usection
         [ ucontainer
             El.
               [p ~at:[At.class' "subtitle"] [txt "DkCoder: Scripting at Scale"]]
         ] ) )

let () =
  if Tr1EntryName.module_id = __MODULE_ID__ then begin
    (* SECTIONS *)
    let andhello_file = AndHello.__FILE__ in
    let andhello_module = AndHello.__MODULE_ID__ in
    title_section () ;
    Y33ArticleX.S004Intro.register_before_tests ~andhello_module () ;
    Y33ArticleX.S008StartScript.register_before_tests ~andhello_module
      ~andhello_file () ;
    Y33ArticleX.S012Reproducib.register_before_tests ~andhello_module () ;
    Y33ArticleX.S016Focus.register_before_tests ~andhello_file
      ~andhelloagain_module:AndHelloAgain.__MODULE_ID__
      ~andhelloagain_file:AndHelloAgain.__FILE__ () ;
    Y33ArticleX.S020RuntimeReqs.register_before_tests () ;
    Y33ArticleX.S022RuntimeSys.register_before_tests () ;
    Y33ArticleX.S024BasShell.register_before_tests
      ~shexpcountdown_module:B43Shell.Index.B35Shexp.B43Countdown.__MODULE_ID__
      ~shexpcountdown_file:B43Shell.Index.B35Shexp.B43Countdown.__FILE__ () ;
    Y33ArticleX.S028PriorArt.register_before_tests () ;
    Y33ArticleX.S032Testing.register_before_tests ~article_module:__MODULE_ID__
      ~article_file:__FILE__ () ;
    Y33ArticleX.S036FlairGraph.register_before_tests
      ~boguetiny_module:B57Graphics.Index.B43Bogue.B43Tiny.__MODULE_ID__
      ~boguetiny_file:B57Graphics.Index.B43Bogue.B43Tiny.__FILE__ () ;
    Y33ArticleX.S038SnokeGame.register_before_tests () ;
    (* Y33ArticleX.S040ReusingYouScripts.register_before_tests () ; *)
    (* Y33ArticleX.S044ListingThirdParties.register_before_tests () ; *)
    (* Y33ArticleX.S048UsingListedThirdParties.register_before_tests () ; *)
    (* Y33ArticleX.S052FunBreak.register_before_tests () ; *)
    (* Y33ArticleX.S056UsingRegThirdParties.register_before_tests () ; *)
    (* Y33ArticleX.S060DkRegistry.register_before_tests () ; *)
    Y33ArticleX.S064MakeProject.register_before_tests () ;
    Y33ArticleX.S066Production.register_before_tests () ;
    Y33ArticleX.S068Parties.register_before_tests () ;
    Y33ArticleX.S072Stdlib.register_before_tests () ;
    Y33ArticleX.S076RuntimeLibs.register_before_tests () ;
    Y33ArticleX.S080InducLimits.register_before_tests () ;
    Y33ArticleX.S084EarlyLimits.register_before_tests () ;
    Y33ArticleX.S088SecDesign.register_before_tests () ;
    Y33ArticleX.S092InTouch.register_before_tests () ;
    (* HTTP SERVER *)
    Y33ArticleX.Httpd.register_before_tests () ;
    (* DOCUMENTATION PRINTING *)
    Y33ArticleX.Doc.register_before_tests () ;
    (* RUN TESTS *)
    Test.run ()
  end
